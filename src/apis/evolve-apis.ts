// let APICore = require("../utilities/api-core").default;
import { StringUtility } from "../utilities/utility";
import { APICore, APIResponse, Method, Options } from "../utilities/api-core";
import { Agent } from "../data-objects/agent";
import { Logger, FunctionType, LogType } from "../utilities/logger";
import { APIVersion } from "../data-objects/cluster";

export default class EvolveAPIs {

	/**
	 * Authorize agent using API then add authorization information back to the agent
	 * @static
	 * @param {Agent} agent which is used to authorize
	 * @param {string} tokenURL URL to get the Token
	 * @param {string} authenticationEncoded Authentication encoded value
	 * @returns {Agent} an authorized agent
	 */
    static async authorize(agent: Agent, tokenURL: string, authenticationEncoded: string): Promise<void> {
        await Logger.write(FunctionType.API, `Authorize Agent ${agent.email}`);

        let options = new Options(tokenURL, Method.POST);
        options.addHeader("Authorization", authenticationEncoded);
        options.addHeader("Content-Type", "application/json; charset=utf-8");
        options.addBody("grant_type", "password");
        options.addBody("username", agent.email);
        options.addBody("password", agent.password);
        options.addBody("scope", "");

        await APICore.request(options).then(
            async (response) => {
                if (response.status == 200) {
                    let jsonbody = JSON.parse(response.body);
                    agent.asscessToken = jsonbody.access_token;
                    agent.baseUri = jsonbody.resource_server_base_uri;
                    agent.tokenType = jsonbody.token_type;
                    agent.agentID = jsonbody.agent_id;
                    agent.teamID = jsonbody.team_id;
                    agent.bussinessNumber = jsonbody.bus_no;
                    await Logger.write(FunctionType.API, `Authorized successfully! Access Token is: ${agent.asscessToken}`);
                } else {
                    throw Error(`Cannot authorize ${agent.email}: Status code: ${response.status}`);
                }
            },
            (error) => { throw <Error>error; }
        );
    }

	/**
	 * Start a new session for an agent using API
	 * @static
	 * @param {Agent} agent which is used to start new session
	 * @param {string} phoneNumber which is used to start new session
	 * @param {string} apiVersion which is used to call API
	 * @returns {APIResponse} response of the API call
	 */
    static async startSession(agent: Agent, phoneNumber: string, apiVersion: string): Promise<APIResponse> {
        await Logger.write(FunctionType.API, `Start session for Agent ${agent.email}`);

        let options = new Options(`${agent.baseUri}services/${apiVersion}/agent-sessions`, Method.POST);
        options.addHeader("Authorization", `${agent.tokenType} ${agent.asscessToken}`);
        options.addHeader("Content-Type", "application/json; charset=utf-8");
        options.addBody("stationId", "");
        options.addBody("stationPhoneNumber", phoneNumber);
        options.addBody("inactivityTimeout", 45);
        options.addBody("inactivityForceLogout", false);

        return await APICore.request(options);
    }

	/**
	 * Join existing session for an agent using API
	 * @static
	 * @param {Agent} agent which is used to join existing session
	 * @param {string} apiVersion which is used to call API
	 * @returns {APIResponse} response of the API call
	 */
    static async joinSession(agent: Agent, apiVersion: string): Promise<APIResponse> {
        await Logger.write(FunctionType.API, `Join existing session for Agent ${agent.email}`);

        let options = new Options(`${agent.baseUri}services/${apiVersion}/agent-sessions/join`, Method.POST);
        options.addHeader("Authorization", `${agent.tokenType} ${agent.asscessToken}`);
        options.addHeader("Content-Type", "application/json; charset=utf-8");
        options.addBody("asAgentId", agent.agentID);

        return await APICore.request(options);
    }

	/**
	 * Start or Join session for an agent using API
	 * @static
	 * @param {Agent} agent which is used to start or join existing session
	 * @param {string} phoneNumber which is used to start or join existing session
	 */
    static async startOrJoinSession(agent: Agent, phoneNumber: string): Promise<void> {
        let startSessionRes = await EvolveAPIs.startSession(agent, phoneNumber, "v11.0");
        if (startSessionRes.status == 202) {
            let jsonbody = JSON.parse(startSessionRes.body);
            agent.sessionId = jsonbody.sessionId;
            await Logger.write(FunctionType.API, `Sesstion started! Session ID is: ${agent.sessionId}`);
        } else if (startSessionRes.status == 409) {
            await Logger.write(FunctionType.API, `Agent ${agent.email} already has an in-progress session. Joining...`);

            let joinRes = await EvolveAPIs.joinSession(agent, "v11.0");
            if (joinRes.status == 202) {
                let jsonbody = JSON.parse(joinRes.body);
                agent.sessionId = jsonbody.sessionId;
                await Logger.write(FunctionType.API, agent.sessionId, LogType.DEBUG);
            }
        } else if(startSessionRes.status == 400){
            throw Error("Phone number or Station in use");
        } 
        else {
            throw Logger.write(FunctionType.API, `Cannot not start or join session for Agent ${agent.email} Error: ${startSessionRes.body}`, LogType.WARN);
        }
    }

	/**
	 * dial a skill for an agent using API
	 * @static
	 * @param {Agent} agent which is used to dial a skill
	 * @param {string} skillName skill name to dial
	 * @param {string} apiVersion which is used to call API
	 * @returns {APIResponse} response of the API call
	 */
    static async dialASkill(agent: Agent, skillName: string, apiVersion: APIVersion): Promise<APIResponse> {
        await Logger.write(FunctionType.API, `Dial ${skillName} skill for Agent ${agent.email}`);
        let url = `${agent.baseUri}services/${apiVersion}/agent-sessions/${agent.sessionId}/dial-skill`;
        let options = new Options(url, Method.POST);
        options.addHeader("Authorization", `${agent.tokenType} ${agent.asscessToken}`);
        options.addHeader("Content-Type", "application/json; charset=utf-8");
        options.addBody("skillName", skillName);

        return await APICore.request(options);
    }

	/**
	 * Make an inbound call for an agent using API
	 * @static
	 * @param {Agent} agent which is used to making inbound call
	 * @param {string} skillName skill name to dial
	 */

    static async makeInboundCall(agent: Agent, skillName: string): Promise<void> {
        let response = await EvolveAPIs.dialASkill(agent, skillName, APIVersion.v2);
        if (response.status == 202) {
            await Logger.write(FunctionType.API, `${skillName} was made for Agent ${agent.email}`);
        } else {
            throw Error(`Cannot dial ${skillName} for Agent ${agent.email}. INFO: ${response.body}`);
        }

    };

    /**
	 * Get Agent skills using API
	 * @static
	 * @param {Agent} agent which is used to get skills
	 * @returns {APIResponse} response of the API call
	 */
    static async getAgentSkills(agent: Agent): Promise<APIResponse> {
        await Logger.write(FunctionType.API, `Getting skills for Agent ${agent.email}`);
        let url = `${agent.baseUri}services/${APIVersion.v4}}/agents/${agent.agentID}/skills`;
        let options = new Options(url, Method.GET);
        options.addHeader("Authorization", `${agent.tokenType} ${agent.asscessToken}`);
        options.addHeader("Content-Type", "application/json; charset=utf-8");
        return await APICore.request(options);
    }

}