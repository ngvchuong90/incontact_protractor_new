import { Config } from 'protractor';
import TestRunInfo from "../data-objects/test-run-info";
import {ConfigReport} from "../utilities/config-report";

let jsLogger = require('js-logger');

export let config: Config = {

    onPrepare: () => {      

        TestRunInfo.setUpTestRunInfo("../../src/test-data/config-info.json");
 
        jsLogger.useDefaults({
            defaultLevel: jsLogger.DEBUG,
            formatter: function (messages, context) {
                messages.unshift(new Date().toLocaleString())
            }
        });

        ConfigReport.createXMLReport();
    },

    onComplete: function () {
        ConfigReport.convertXMLtoPieChart();
    },
  
    framework: 'jasmine',
    capabilities: {
        browserName: 'chrome',
    },

    seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: true,
    SELENIUM_PROMISE_MANAGER: false,
    // restartBrowserBetweenTests: true,

    specs: [
        '../testcases/evolve/evolve-impersonate-and-configure.js',
        '../testcases/evolve/evolve-inbound-call.js',
        '../testcases/evolve/evolve-create-employee.js',
        '../testcases/evolve/evolve-create-tenant.js',
        '../testcases/evolve/evolve-outbound-call.js'
    ]
};