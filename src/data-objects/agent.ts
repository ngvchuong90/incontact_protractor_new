import { StringUtility } from "../utilities/utility";
import { JsonObject, JsonProperty } from "json2typescript";
import TestRunInfo from "./test-run-info";

export enum AgentType {
	SuperAdmin = "SuperAdmin",
	Admin = "Admin",
	Agent = "Agent"
}

export enum AgentRole {
	Administrator = "Administrator",
	Agent = "Agent",
	Evaluator = "Evaluator",
	Manager = "Manager"
}

@JsonObject
export class Agent {

	@JsonProperty("type", String)
	type: string = undefined;

	@JsonProperty("email", String)
	email: string = undefined;

	@JsonProperty("password", String)
	password: string = undefined;

	@JsonProperty("name", String)
	name: string = undefined;

	phoneNumber: string;
	asscessToken: string;
	baseUri: string;
	tokenType: string;
	agentID: string;
	teamID: string;
	bussinessNumber: string;
	sessionId: string;
	firstname: string;
	lastname: string;
	role: AgentRole;

	initData(role: AgentRole) {
		this.password = "Password1";
		this.firstname = "lgvn";
		this.lastname = StringUtility.createRandomString(12, "test");
		this.email = "lgvn_" + this.lastname + "@mailinator.com";
		this.name = this.firstname + " " + this.lastname;
		this.role = role;
		this.phoneNumber = StringUtility.createEvolveRandomPhone(TestRunInfo.clusterID);
		this.asscessToken;
		this.baseUri;
		this.tokenType;
		this.agentID;
		this.teamID;
		this.bussinessNumber;
		this.sessionId;
		return this;
	}

	// initCreateData(role: AgentRole) {
	// 	this.firstname = "lgvn";
	// 	this.lastname = StringUtility.createRandomString(12, "test");
	// 	this.role = role;
	// 	this.email = "lgvn_" + this.lastname + "@mailinator.com";
	// 	return this;
	// }

	// this.firstname = "LGVN";
	// this.lastname = StringUtility.createRandomString(12, "test");
	// this.role = role;
	// this.emailaddress = "lgvn_" + this.lastname + "@mailinator.com";
	// return this;
}