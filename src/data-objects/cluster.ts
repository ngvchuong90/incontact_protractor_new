import { JsonObject, JsonProperty } from "json2typescript";
import { Agent, AgentType } from "../data-objects/agent";
import { StringUtility } from "../utilities/utility";
import TestRunInfo from "./test-run-info";

@JsonObject
export class Cluster {
    @JsonProperty("id", String)
    id: string = undefined;
    @JsonProperty("encoded", String)
    encoded: string = undefined;
    @JsonProperty("tokenUrl", String)
    tokenUrl: string = undefined;
}

@JsonObject
export class EvolveCluster extends Cluster {
    @JsonProperty("agents", [Agent])
    agents: Agent[] = undefined;
    @JsonProperty("outboundNumber", String)
    outboundNumber: string = undefined;
    @JsonProperty("inboundNumber", String)
    inboundNumber: string = undefined;
    @JsonProperty("loginUrl", String)
    loginUrl: string = undefined;
    @JsonProperty("tenantUrl", String)
    tenantUrl: string = undefined;
    @JsonProperty("evolveUrl", String)
    evolveUrl: string = undefined;
    @JsonProperty("evolveHomeUrl", String)
    evolveHomeUrl: string = undefined;
    @JsonProperty("activateUrl", String)
    activateUrl: string = undefined;
    @JsonProperty("Ibskill", String)
    Ibskill: string = undefined;
    @JsonProperty("Obskill", String)
    Obskill: string = undefined;
    @JsonProperty("tenantName", String)
    tenantName: string = undefined;

    constructor() { super(); }

    /**
     * Get agent information
     * @param {AgentType} type agent type
     * @returns {Agent} result information of given agent
     * @memberof EvolveCluster
     */
    public getAgent(type: AgentType): Agent[] {
        let result:Agent[] = new Array();
        let i;
        let length = this.agents.length;
        let crAgent:Agent;

        for (i = 0; i < length; i++) {
            crAgent = this.agents[i];
            if (crAgent.type == type.toString()) {
                crAgent.phoneNumber = StringUtility.createEvolveRandomPhone(TestRunInfo.cluster.id);
                result.push(crAgent);
            }
        }

        return result;
    }

    public getAdmin(): Agent {
        return this.getAgent(AgentType.Admin)[0];
    }

    public getSuperAdmin(): Agent {
        return this.getAgent(AgentType.SuperAdmin)[0];
    }

    public getRandomAgent(): Agent {
        return this.getAgent(AgentType.Agent)[StringUtility.getRandomNumber(1, 0, 2)];
    }
}

@JsonObject
export class EvolveClusters {
    @JsonProperty("evolveClusters", [EvolveCluster])
    private evolveClusters: EvolveCluster[] = undefined;

    /**
     * Get cluster information
     * @param {ClusterID} id cluster id
     * @returns {EvolveCluster} result information of cluster
     * @memberof EvolveClusters
     */
    public getCluster(id: ClusterID): EvolveCluster {
        let result: EvolveCluster;

        for (let i = 0; i < this.evolveClusters.length; i++) {
            if (this.evolveClusters[i].id == id.toString()) {
                result = this.evolveClusters[i];
                break;
            }
        }

        return result;
    }

}

export enum ClusterID {
    TO31 = "TO31",
    TO32 = "TO32",
    SO31 = "SO31",
    SO32 = "SO32",
    C32 = "C32"
}

export enum MaxState {
    AVAILABLE = "Available",
    UNAVAILABLE = "Unavailable",
    LOGOUT = "Logout"
}

export enum MaxConnectOption {
    PHONE = "Phone",
    STATIONID = "Station ID",
    SOFTPHONE = "Integrated Softphone",
}

export enum APIVersion {
    v1 = "v1.0",
    v2 = "v2.0",
    v3 = "v3.0",
    v4 = "v4.0",
    v5 = "v5.0",
    v6 = "v6.0",
    v7 = "v7.0",
    v8 = "v8.0",
    v9 = "v9.0",
    v10 = "v10.0",
    v11 = "v11.0",
    v12 = "v12.0"
}

export enum SearchTimeRange {
    TODAY = "Today",
    LAST_2_DAYS = "Last 2 days",
    LAST_7_DAYS = "Last 7 days",
    CURRENT_MONTH = "Current month",
    PREVIOUS_MONTH = "Previous month",
    CUSTOM_RANGE = "Custom range",
    LAST_DAYS = "Last ... days"
}

export enum SearchColumnName {
    TYPE = "TYPE",
    AGENT_NAME = "AGENT NAME",
    CUSTOMER_INFO = "CUSTOMER INFO",
    START_TIME = "START TIME",
    DURATION = "DURATION",
    DIRECTION = "DIRECTION",
    ORGANIZATION_INFO = "ORGANIZATION INFO",
    EVALUATION_SCORE = "EVALUATION SCORE",
    CALL_REASON = "CALL REASON",
    SKILLS = "SKILLS",
    SEGMENT_ID = "SEGMENT ID",
    CONTACT_ID = "CONTACT ID",
    AGENT_ID = "AGENT ID",
    ACD_ID = "ACD ID",
    EMAIL_SUBJECT = "EMAIL SUBJECT"
}

export enum Timezone {
    PACIFIC_TIME = "(UTC-07:00) Pacific Time (US & Canada)",
    MOUTAIN_TIME = "(UTC-06:00) Mountain Time (US & Canada)",
    CENTRAL_TIME = "(UTC-05:00) Central Time (US & Canada)",
    BANGKOK_HANOI_JAKATA = "(UTC+07:00)  Bangkok, Hanoi, Jakarta"
}