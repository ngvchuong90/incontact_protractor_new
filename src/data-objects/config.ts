import { JsonObject, JsonProperty } from "json2typescript";
import TestRunInfo from "./test-run-info";

@JsonObject
export class ConfigInfo {
    @JsonProperty("clusterId", String)
    clusterId: string = undefined;
    @JsonProperty("browser", String)
    browser: string = undefined;
    @JsonProperty("elementTimeout", Number)
    elementTimeout: number = undefined;
    @JsonProperty("pageTimeout", Number)
    pageTimeout: number = undefined;
    @JsonProperty("testTimeout", Number)
    testTimeout: number = undefined;
}