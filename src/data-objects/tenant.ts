/**********************************************
 IMPORTS
 **********************************************/
import { StringUtility } from "../utilities/utility";

export class Tenant {
	name: string;
	partner: string;
	tenantType: string;
	expirationDate: string;
	timezone: string;
	billingId: string;
	clusterId: string;
	billingCycle: number;
	billingTelephoneNumber: string;
	userCap: number;

	initData(tenantType: string, timezone: string, billingId: string, clusterId: string, partner: string = "", expirationDate: string = "") {
		let _tenantName = StringUtility.createRandomString(18, "IT_lgvn_");
		let _phoneNumber = "99" + StringUtility.getRandomNumber(8);
		this.name = _tenantName;
		this.partner = partner;
		this.tenantType = tenantType;
		this.expirationDate = expirationDate;
		this.timezone = timezone;
		this.billingId = billingId;
		this.clusterId = clusterId;
		this.billingCycle = StringUtility.getRandomNumber(null, 1, 28);
		this.billingTelephoneNumber = _phoneNumber;
		this.userCap = StringUtility.getRandomNumber(null, 1, 3000);
	}
}

export enum TenantType{
	TRIAL = "TRIAL",
	CUSTOMER = "CUSTOMER"
}

export enum ApplicationType{
	ACD = "ACD",
	ANALYTICS = "Analytics",
	PM = "PM",
	QM = "QM"
}

export enum Feature{
	QM="QM"
}

export enum RecordingSetting{
	CXONE = "CXone",
	AMAZON_CONNECT = "Amazon Connect",
	UPTIVITY = "Uptivity"
}



