import { ClusterID, Cluster, EvolveCluster, EvolveClusters } from "../data-objects/cluster";
import * as fs from 'fs';
import { ConfigInfo } from "../data-objects/config"
import { JsonUtility } from "../utilities/utility";
import { evolveClusters } from "../test-data/evolve-cluster-info";

export default class TestRunInfo {
    static browser: string;
    static clusterID: ClusterID;
    static cluster: EvolveCluster;
    static elementTimeout: number;
    static pageTimeout: number;
    static testTimeout: number;

    public static setUpTestRunInfo(jsonPath: string): void{
        let jsonString: string = fs.readFileSync(jsonPath, 'utf8');
        let configData: ConfigInfo = JsonUtility.deserialize(JSON.parse(jsonString), ConfigInfo);

        TestRunInfo.clusterID = <ClusterID>configData.clusterId;
        TestRunInfo.browser = configData.browser;
        TestRunInfo.cluster = (<EvolveClusters>JsonUtility.deserialize(evolveClusters, EvolveClusters)).getCluster(TestRunInfo.clusterID);
        TestRunInfo.elementTimeout = configData.elementTimeout;
        TestRunInfo.pageTimeout = configData.pageTimeout;
        TestRunInfo.testTimeout = configData.testTimeout;
    }
}