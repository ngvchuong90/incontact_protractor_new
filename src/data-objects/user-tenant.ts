/**********************************************
 IMPORTS
 **********************************************/
import { StringUtility } from "../utilities/utility";

/**********************************************
 PRIVATE VARIABLES
 **********************************************/

export default class UserTenant {
	firstName: string;
	lastName: string;
	username: string;
	email: string;
	password: string;
	confirmPassword: string;

	initData(username: string) {
		let randomPassword = StringUtility.createRandomString(10,"Lgvn");
		this.username = username + "@mailinator.com";
		this.firstName = "lgvn_first";
		this.lastName = StringUtility.createRandomString(18, "lgvn_last");
		this.email = this.username;
		this.password = randomPassword;
		this.confirmPassword = randomPassword;
	}
}