import { by, promise } from "protractor";
import ElementWrapper from "../../../utilities/protractor-wrappers/element-wrapper";
import { Logger, FunctionType } from "../../../utilities/logger";
import ACDSkillPage from "./acd-skill-page";

export default class ACDPage {

    private static _acdPage: ACDPage = null;

    protected acdSkillsMenu = new ElementWrapper(by.xpath("//a[@id='SkillsList-link']"));

    public static getInstance(): ACDPage {
        // this._acdPage = (this._acdPage == null) ? new ACDPage() : this._acdPage;
        this._acdPage = new ACDPage();
        return this._acdPage;
    }

    /**
     * Open "ACD Skill" page by clicking "ACD Skill" menu
     * @returns {Promise<ACDSkillPage>} "ACD skill" page 
     * @memberof ACDPage
     */
    public async openACDSkillsPage(): Promise<ACDSkillPage> {
        await Logger.write(FunctionType.UI, "Going to ACD Skills page");
        await this.acdSkillsMenu.click();

        return ACDSkillPage.getInstance();
    }
}