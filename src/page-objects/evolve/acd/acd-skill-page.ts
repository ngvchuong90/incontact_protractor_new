import { by } from "protractor";
import { Logger, LogType, FunctionType } from '../../../utilities/logger';
import ElementWrapper from "../../../utilities/protractor-wrappers/element-wrapper";
import SkillsDetailPage from "./skills-detail";


export default class ACDSkillPage {

    private static _acdSkillPage: ACDSkillPage = null;

    protected txtSearch = new ElementWrapper(by.xpath("//input[@name='ctl00$ctl00$ctl00$BaseContent$Content$ManagerContent$agvsSkills$tbxSearchText']"));
    protected btnSearch = new ElementWrapper(by.xpath("//input[@id='ctl00_ctl00_ctl00_BaseContent_Content_ManagerContent_agvsSkills_btnSearch']"));
    protected userTab = new ElementWrapper(by.xpath("//a[@id='__tab_ctl00_ctl00_ctl00_BaseContent_Content_ManagerContent_tcSkillDetails_tpnlSkillsAgents']"));
    protected btnEdit = new ElementWrapper(by.xpath("//span[@id='ctl00_ctl00_ctl00_BaseContent_Content_ManagerContent_tcSkillDetails_tpnlSkillDetails_btnEdit_ShadowButtonSpan']"));
    protected btnDone = new ElementWrapper(by.xpath("//span[@id='ctl00_ctl00_ctl00_BaseContent_Content_ManagerContent_tcSkillDetails_tpnlSkillDetails_btnUpdate_btnMain_ShadowButtonSpan']"));
    protected btnDiscard = new ElementWrapper(by.xpath("//span[@id='ctl00_ctl00_ctl00_BaseContent_Content_ManagerContent_tcSkillDetails_tpnlSkillDetails_btnDiscardChanges_ShadowButtonSpan']"));
    protected spLoading = new ElementWrapper(by.xpath("//div[@id='spinner_main_component']"));

    protected _getSkill(skillname): ElementWrapper {
        return new ElementWrapper(by.xpath(`//table[@id='ctl00_ctl00_ctl00_BaseContent_Content_ManagerContent_agvsSkills_gridView']//td[text()='${skillname}']`));
    }

    public static getInstance() {
        // this._acdSkillPage = (this._acdSkillPage == null) ? new ACDSkillPage() : this._acdSkillPage;
        this._acdSkillPage = new ACDSkillPage();
        return this._acdSkillPage;
    }

    /**
     * Select skill in "ACD skills" page
     * @param {string} skillname name of ACD skill
     * @returns {Promise<SkillsDetailPage>} "Skill details" page
     * @memberof ACDSkillPage
     */
    public async selectSkill(skillname): Promise<SkillsDetailPage> {
        await Logger.write(FunctionType.UI, "Selecting skill");
        await this.txtSearch.type(skillname);
        await this.btnSearch.click();
        await this.spLoading.waitUntilPropertyNotChange("style");
        await this._getSkill(skillname).click();

        let SkillsDetails = require("./skills-detail").default;
        return SkillsDetailPage.getInstance();
    }

    /**
     * Go to "user" tab by clicking "User" tab
     * @memberof ACDSkillPage
     */
    public async gotoUserTab() {
        await Logger.write(FunctionType.UI, "Going to User tab");
        await this.userTab.click();
    }

}