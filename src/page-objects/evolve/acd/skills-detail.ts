import { by } from "protractor";
import ElementWrapper from "../../../utilities/protractor-wrappers/element-wrapper";
import TopMenu from '../../evolve/top-menu';
import { Logger, LogType, FunctionType } from '../../../utilities/logger';

export default class SkillsDetailPage extends TopMenu {

	private static _skillsDetailPage: SkillsDetailPage = null;

	protected tabUser = new ElementWrapper(by.xpath("//span[contains(@id,'tcSkillDetails_tpnlSkillsAgents_tab')]"));
	protected txtAssignSearch = new ElementWrapper(by.xpath("//input[@id='ctl00_ctl00_ctl00_BaseContent_Content_ManagerContent_tcSkillDetails_tpnlSkillsAgents_SkillsAgents_gsUnassigned_tbSearchCriteria']"));
	protected btnAssignSearch = new ElementWrapper(by.xpath("//button[@id='ctl00_ctl00_ctl00_BaseContent_Content_ManagerContent_tcSkillDetails_tpnlSkillsAgents_SkillsAgents_gsUnassigned_btnSearch_ShadowButton']"));
	protected btnAddUsers = new ElementWrapper(by.xpath("//button[contains(@id,'tcSkillDetails_tpnlSkillsAgents_SkillsAgents_btnAddAgents_ShadowButton')]"));
	protected spLoading = new ElementWrapper(by.xpath("//div[@id='spinner_main_component']"));

	protected getAssignCheckbox(text: string): ElementWrapper {
		return new ElementWrapper(by.xpath(`//td[text()='${text}']/preceding-sibling::td/span[@class='checkbox']`));
	}

	constructor() { super(); }

	public static getInstance(): SkillsDetailPage {
		// this._skillsDetailPage = (this._skillsDetailPage == null) ? new SkillsDetailPage() : this._skillsDetailPage;
		this._skillsDetailPage = new SkillsDetailPage();
		return this._skillsDetailPage;
	}

	/**
	 * Go to "User" tab in "Skill Details" page
	 * @memberof SkillsDetailPage
	 */
	public async gotoUserTab() {
		await Logger.write(FunctionType.UI, "Going to User tab");
		await this.tabUser.click();
	}

	/**
	 * Assign skill for user
	 * @param {string} email email of user
	 * @returns {Promise<this>} "Skill Detail" page
	 * @memberof SkillsDetailPage
	 */
	public async assignUserSkill(email: string): Promise<this> {
		await this.txtAssignSearch.type(email);
		await this.btnAssignSearch.click();
		await new ElementWrapper(this.getAssignCheckbox(email)).click();
		await this.btnAddUsers.click();
		await this.spLoading.waitUntilPropertyNotChange("style");
		return this;
	}

}
