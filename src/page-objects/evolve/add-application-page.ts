import { by } from "protractor";
import { Logger, LogType, FunctionType } from "../../utilities/logger";
import ElementWrapper from "../../utilities/protractor-wrappers/element-wrapper";
import CreateTenantPage from "./create-tenant-page";

export default class AddApplicationPage {

    private static _addApplicationPage: AddApplicationPage = null;

    protected lblAddApplication = new ElementWrapper(by.xpath("//div[@class='modal-title ng-binding']"));
    protected bodyQMSetting = new ElementWrapper(by.xpath("//div[contains(@class,'settings-container')]"));
    protected btnNext = new ElementWrapper(by.xpath("//button[contains(@class,'next')]"));
    protected btnBack = new ElementWrapper(by.xpath("//button[contains(@class,'back')]"));
    protected btnCancel = new ElementWrapper(by.xpath("//button[contains(@class,'cancel')]"));
    protected chbQM = new ElementWrapper(by.xpath("//label[@class='feature-checkbox' and (.//span[text()='QM'])]/div"));
    protected cbRecordingSource = new ElementWrapper(by.xpath("//div[@class='field'][./label[text()='Recording Source']]//div[contains(@class,'nice-dropdown')]"));
    protected addApplicationForm = new ElementWrapper(by.xpath("//div[@class='modal fade ng-scope ng-isolate-scope add-application-modal in']"));

    public static getInstance(): AddApplicationPage {
        // this._addApplicationPage = (this._addApplicationPage == null) ? new AddApplicationPage() : this._addApplicationPage;
        this._addApplicationPage = new AddApplicationPage();
        return this._addApplicationPage;
    }

    protected getRecordingSourceItem(recordingSource: string): ElementWrapper {
        return new ElementWrapper((by.xpath(`//div[contains(@class,'nice-dropdown')][.//span[text()='${recordingSource}']]`)));
    }

    protected getApplicationItem(application: string): ElementWrapper {
        return new ElementWrapper((by.xpath(`//input[@type='radio']/following-sibling::label[text()='${application}']`)));
    }

    /**
     * Check "Add Application" page is displayed
     * @returns {Promise<boolean>} Display=>true, not displayed=>false
     * @memberof AddApplicationPage
     */
    public async isPageDisplayed(): Promise<boolean> {
        return await this.lblAddApplication.isDisplayed();
    }

    /**
     * Select application in "Add Application" page
     * @param {string} application application type
     * @returns {Promise<AddApplicationPage>} Add Application page
     * @memberof AddApplicationPage
     */
    public async selectApplication(application: string): Promise<AddApplicationPage> {
        await Logger.write(FunctionType.UI, "Select Application");
        await this.getApplicationItem(application).click();
        return this;
    }

    /**
     * Go to "Feature" tab by clicking "Next" button
     * @returns {Promise<AddApplicationPage>} Add Application page
     * @memberof AddApplicationPage
     */
    public async gotoFeatureTab(): Promise<AddApplicationPage> {
        await Logger.write(FunctionType.UI, "Going to Select Feature Tab");
        await this.btnNext.click();
        return this;
    }

    /**
     * Check "QM" checkbox is displayed
     * @returns {Promise<boolean>}  Display=>true, not displayed=>false
     * @memberof AddApplicationPage
     */
    public async isQmEvolveCheckboxChecked(): Promise<boolean> {
        let attributeClass = "class";
        let chbClass = await this.chbQM.getAttribute(attributeClass);
        return (chbClass.includes("ng-not-empty"));
    }

    /**
     * Go to "Configure Setting" tab by clicking "Next" button
     * @returns {Promise<AddApplicationPage>} Add Application page
     * @memberof AddApplicationPage
     */
    public async gotoConfigureSettingsTab(): Promise<AddApplicationPage> {
        await Logger.write(FunctionType.UI, "Going to Configure Settings Tab");
        await this.btnNext.click();
        return this;
    }

    /**
     * Check "Qm Setting" page is displayed
     * @returns {Promise<boolean>} the existence of "Qm Setting" page
     * @memberof AddApplicationPage
     */
    public async isQMSettingPageDisplayed(): Promise<boolean> {
        await Logger.write(FunctionType.UI, "Checking If QM Setting Page Displays");
        return await this.bodyQMSetting.isDisplayed();
    }

    /**
     * Select recording source 
     * @param {string} recordingSource recording source type
     * @returns {Promise<AddApplicationPage>} Add Application page
     * @memberof AddApplicationPage
     */
    public async selectRecordingSource(recordingSource: string): Promise<AddApplicationPage> {
        await Logger.write(FunctionType.UI, "Selecting Recording Source");
        await this.cbRecordingSource.click();
        await this.getRecordingSourceItem(recordingSource).click();
        return this;
    }

    /**
     * Check the "Config Setting" is selected
     * @param {string} recordingSource name of recording source
     * @returns {Promise<boolean>} the state of config setting in "QM Setting" page
     * @memberof AddApplicationPage
     */
    public async isConfigSettingSelected(recordingSource: string): Promise<boolean> {
        await Logger.write(FunctionType.UI, "Checking Config Setting");
        return await this.getRecordingSourceItem(recordingSource).isDisplayed();
    }

    /**
     * Complete Add Application by clicking "Finish" button
     * @returns {Promise<CreateTenantPage>} "Create Tenant" page
     * @memberof AddApplicationPage
     */
    public async finishAddApplication(): Promise<CreateTenantPage> {
        await Logger.write(FunctionType.UI, "Finishing Add Application");
        await this.btnNext.click();
        await this.btnNext.waitUntilDisappear();
        await this.addApplicationForm.waitUntilDisappear();
        return CreateTenantPage.getInstance();
    }
}