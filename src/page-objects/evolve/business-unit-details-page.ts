import { by } from "protractor";
import TopMenu from "../../page-objects/evolve/top-menu";
import { Logger, LogType, FunctionType } from "../../utilities/logger";
import ElementWrapper from "../../utilities/protractor-wrappers/element-wrapper";

export default class BusinessUnitDetailsPage extends TopMenu {

    constructor() { super(); }

    private static _businessUnitDetails: BusinessUnitDetailsPage = null;

    public static getInstance(): BusinessUnitDetailsPage {
        // this._businessUnitDetails = (this._businessUnitDetails == null) ? new BusinessUnitDetailsPage() : this._businessUnitDetails;
        this._businessUnitDetails = new BusinessUnitDetailsPage();
        return this._businessUnitDetails;
    }

    protected tabBusinessUnitDetails = new ElementWrapper(by.xpath("//div[@id='ctl00_ctl00_ctl00_BaseContent_Content_ManagerContent_tcBusinessUnit_tabDetails']"));

    /**
     * Check "Business Unit Details" page is displayed or not
     * @returns {Promise<boolean>} Display=>true, not displayed=>false
     * @memberof BusinessUnitDetailsPage
     */
    public async isBusinessUnitDetailsPageDisplayed(): Promise<boolean> {
        await Logger.write(FunctionType.UI, "Checking If Business Unit Details Page Displays");
        return await this.tabBusinessUnitDetails.isDisplayed();
    }

}
