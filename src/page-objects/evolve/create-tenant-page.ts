import { by } from "protractor";
import { Logger, LogType, FunctionType } from "../../utilities/logger";
import ElementWrapper from "../../utilities/protractor-wrappers/element-wrapper";
import { Tenant, TenantType } from "../../data-objects/tenant";
import { Agent } from "../../data-objects/agent";
import TenantPage from "./tenant-page";
import AddApplicationPage from "./add-application-page";
import UserTenant from "../../data-objects/user-tenant";
import BrowserWrapper from "../../utilities/protractor-wrappers/browser-wrapper";

export default class CreateTenantPage {

  private static _createTenantPage: CreateTenantPage = null;
  private attributeValue: string = "value";

  protected lblCreateTenant = new ElementWrapper(by.xpath("//div[@class='container page-container create-tenant-data']//h1[@class='page-title ng-binding']"));
  protected tabGeneral = new ElementWrapper(by.xpath("//li[@id='general-tab']"));
  protected tabApplicationsFeature = new ElementWrapper(by.xpath("//li[@id='applications-tab']"));
  protected tabUsers = new ElementWrapper(by.xpath("//li[@id='users-tab']"));
  protected txtName = new ElementWrapper(by.xpath("//input[@name='tenantName']"));
  protected txtExpirationDate = new ElementWrapper(by.xpath("//input[@name='expirationDate']"));
  protected txtBillingId = new ElementWrapper(by.xpath("//input[@name='billingId']"));
  protected txtClusterId = new ElementWrapper(by.xpath("//input[@name='clusterId']"));
  protected txtBillingCycle = new ElementWrapper(by.xpath("//input[@name='billingCycle']"));
  protected txtBillingTelephoneNumber = new ElementWrapper(by.xpath("//input[@name='billingTelephoneNumber']"));
  protected txtUserCap = new ElementWrapper(by.xpath("//input[@name='userSoftLimit']"));
  protected txtFirstName = new ElementWrapper(by.xpath("//input[@name='firstName']"));
  protected txtLastName = new ElementWrapper(by.xpath("//input[@name='lastName']"));
  protected txtUsername = new ElementWrapper(by.xpath("//input[@name='username']"));
  protected txtEmailAddress = new ElementWrapper(by.xpath("//input[@name='emailAddress']"));
  protected txtPassword = new ElementWrapper(by.xpath("//input[@name='password']"));
  protected txtConfirmPassword = new ElementWrapper(by.xpath("//input[@name='confirmPassword']"));
  protected cbxTimeZone = new ElementWrapper(by.xpath("//div[contains(@data-options,'timeZones')]"));
  protected cbxTenantType = new ElementWrapper(by.xpath("//div[contains(@data-options,'tenantTypes')]"));
  protected btnCreateActive = new ElementWrapper(by.xpath("//button[@id='create-tenant-create-btn']"));
  protected btnCancel = new ElementWrapper(by.xpath("//button[contains(@class,'cancel')]"));
  protected btnAddApplication = new ElementWrapper(by.xpath("//button[@id='add-application-btn']"));
  protected btnNext = new ElementWrapper(by.xpath("//button[contains(@class,'next')]"));
  protected rowSpinner = new ElementWrapper(by.xpath('//div[@class="row wfmspinner"]'));

  protected getTenantTypeItem(tenantType: string): ElementWrapper {
    return new ElementWrapper(by.xpath(`//div[contains(@data-options,'tenantTypes')]//span[text()='${tenantType}']`));
  }

  protected getTimeZoneItem(timeZone: string): ElementWrapper {
    return new ElementWrapper(by.xpath(`//div[contains(@data-options,'timeZones')]//span[contains(text(),'${timeZone}')]`));
  }

  protected getApplication(application: string): ElementWrapper {
    return new ElementWrapper(by.xpath(`//div[contains(@class,'application') and @id='${application}']`));
  }

  public static getInstance(): CreateTenantPage {
    // this._createTenantPage = (this._createTenantPage == null) ? new CreateTenantPage() : this._createTenantPage;
    this._createTenantPage = new CreateTenantPage();
    return this._createTenantPage;
  }

  /**
   * Check "Create Tenant" page is displayed
   * @returns {Promise<boolean>} Display=>true, not displayed=>false
   * @memberof CreateTenantPage
   */
  public async isPageDisplayed(): Promise<boolean> {
    await Logger.write(FunctionType.UI, "Checking If Create Tenant Page Displays");
    return await this.lblCreateTenant.isDisplayed();
  }

  /**
   * Go to "General" tab by clicking "General" tab
   * @returns {Promise<CreateTenantPage>} Create Tenant page
   * @memberof CreateTenantPage
   */
  public async gotoGeneralTab(): Promise<CreateTenantPage> {
    await Logger.write(FunctionType.UI, "Going to General Tab");
    await this.tabGeneral.click();
    return this;
  }

  /**
   * Go to "Applications & Features" tab by clicking "Applications & Features" tab
   * @returns {Promise<CreateTenantPage>} Create Tenant page
   * @memberof CreateTenantPage
   */
  public async gotoApplicationsFeaturesTab(): Promise<CreateTenantPage> {
    await Logger.write(FunctionType.UI, "Going to Applications & Features Tab");
    await this.tabApplicationsFeature.click();
    return this;
  }

  /**
   * Go to "Users" tab by clicking "Users" tab
   * @returns {Promise<CreateTenantPage>} Create Tenant page
   * @memberof CreateTenantPage
   */
  public async gotoUsersTab(): Promise<CreateTenantPage> {
    await Logger.write(FunctionType.UI, "Going to Users Tab");
    await this.tabUsers.click();
    return this;
  }

  /**
   * Select tenant type
   * @param {string} tenantType type of tenant to select
   * @returns {Promise<CreateTenantPage>} Create Tenant page
   * @memberof CreateTenantPage
   */
  public async selectTenantType(tenantType: string): Promise<CreateTenantPage> {
    await Logger.write(FunctionType.UI, "Selecting Tenant Type");
    await this.cbxTenantType.click();
    await this.getTenantTypeItem(tenantType).click();
    return this;
  }

  /**
   * Select timezone
   * @param {string} timeZone timezone to select
   * @returns {Promise<CreateTenantPage>} "Create Tenant" page
   * @memberof CreateTenantPage
   */
  public async selectTimeZone(timeZone: string): Promise<CreateTenantPage> {
    await Logger.write(FunctionType.UI, "Selecting TimeZone");
    await this.cbxTimeZone.click();
    await this.getTimeZoneItem(timeZone).click();
    return this;
  }

  /**
   * Open "Add Application" page by clicking "Add Application" button
   * @returns {Promise<AddApplicationPage>} "Add Application" page
   * @memberof CreateTenantPage
   */
  public async addApplication(): Promise<AddApplicationPage> {
    await Logger.write(FunctionType.UI, "Going to Add Application Page");
    await this.btnAddApplication.click();
    return AddApplicationPage.getInstance();
  }

  /**
   * Fill data in "General" tab
   * @param {Tenant} tenant information of tenant to create 
   * @returns {Promise<CreateTenantPage>} "Create Tenant" page
   * @memberof CreateTenantPage
   */
  public async fillGeneralTab(tenant: Tenant): Promise<CreateTenantPage> {
    await Logger.write(FunctionType.UI, "Filling in General Tab");
    await this.txtName.type(tenant.name); 
    await this.selectTenantType(tenant.tenantType);
    if (tenant.tenantType != TenantType.CUSTOMER) {
      await this.txtExpirationDate.type(tenant.expirationDate);
    }
    await this.selectTimeZone(tenant.timezone);
    await this.txtBillingId.type(tenant.billingId);
    await this.rowSpinner.waitUntilDisappear();
    await this.txtClusterId.type(tenant.clusterId);
    await this.txtBillingCycle.type(tenant.billingCycle);
    await this.txtBillingTelephoneNumber.type(tenant.billingTelephoneNumber);
    await this.txtUserCap.type(tenant.userCap);
    await BrowserWrapper.executeScript('window.scrollTo(0,0);');
    return this;
  }

  /**
   * Fill data in "users" tab
   * @param {UserTenant} user user information to create 
   * @returns {Promise<CreateTenantPage>} "Create Tenant" page
   * @memberof CreateTenantPage
   */
  public async fillUserTab(user: UserTenant): Promise<CreateTenantPage> {
    await Logger.write(FunctionType.UI, "Filling in Users Tab");
    await this.txtFirstName.type(user.firstName);
    await this.txtLastName.type(user.lastName);
    await this.txtUsername.type(user.email);
    await this.rowSpinner.waitUntilDisappear();
    await this.txtEmailAddress.type(user.email);
    await this.txtPassword.type(user.password);
    await this.txtConfirmPassword.type(user.password);
    await BrowserWrapper.executeScript('window.scrollTo(0,0);');
    return this;
  }

  /**
   * Get entered tenant name
   * @returns {Promise<string>} inputted tenant name
   * @memberof CreateTenantPage
   */
  public async getEnteredName(): Promise<string> {
    let name = await this.txtName.getAttribute(this.attributeValue);
    return name;
  }

  /**
   * Get selected tenant type
   * @returns {Promise<string>} inputted tenant type
   * @memberof CreateTenantPage
   */
  public async getSelectedTenantType(): Promise<string> {
    let tenantType = await this.cbxTenantType.getText();
    return tenantType;
  }

  /**
   * Get entered expiration date is entered
   * @returns {Promise<string>} inputted expiration date 
   * @memberof CreateTenantPage
   */
  public async getEnteredExpirationDate(): Promise<string> {
    let expirationDate = await this.txtExpirationDate.getAttribute(this.attributeValue);
    return expirationDate;
  }

  /**
   * Get selected timezone
   * @returns {Promise<string>} inputted timezone
   * @memberof CreateTenantPage
   */
  public async getSelectedTimezone(): Promise<string> {
    let timezone = await this.cbxTimeZone.getText();
    return timezone;
  }

  /**
   * Get entered billing id
   * @returns {Promise<number>} inputted billing Id
   * @memberof CreateTenantPage
   */
  public async getEnteredBillingId(): Promise<string> {
    let billingId = await this.txtBillingId.getAttribute(this.attributeValue);
    return billingId;
  }

  /**
   * Get entered cluster id
   * @returns {Promise<number>} inputted cluster Id
   * @memberof CreateTenantPage
   */
  public async getEnteredClusterId(): Promise<string> {
    let clusterId = await this.txtClusterId.getAttribute(this.attributeValue);
    return clusterId;
  }

  /**
   * Get entered billing cycle
   * @returns {Promise<number>} inputted billing cycle
   * @memberof CreateTenantPage
   */
  public async getEnteredBillingCycle(): Promise<number> {
    let billingCycle = await this.txtBillingCycle.getAttribute(this.attributeValue);
    return parseInt(billingCycle);
  }

  /**
   * Get entered billing telephone number
   * @returns {Promise<string>} inputted billing telephone number
   * @memberof CreateTenantPage
   */
  public async getEnteredBillingTelephoneNumber(): Promise<string> {
    let billingTelephoneNumber = await this.txtBillingTelephoneNumber.getAttribute(this.attributeValue);
    return billingTelephoneNumber;
  }

  /**
   * Get entered user cap
   * @returns {Promise<number>} inputted user cap
   * @memberof CreateTenantPage
   */
  public async getEnteredUserCap(): Promise<number> {
    let userCap = await this.txtUserCap.getAttribute(this.attributeValue);
    return parseInt(userCap);
  }

  /**
   * Get entered first name
   * @returns {Promise<string>} inputted first name
   * @memberof CreateTenantPage
   */
  public async getEnteredFirstName(): Promise<string> {
    let firstName = await this.txtFirstName.getAttribute(this.attributeValue);
    return firstName;
  }

  /**
   * Get entered last name
   * @returns {Promise<string>} inputted last name
   * @memberof CreateTenantPage
   */
  public async getEnteredLastName(): Promise<string> {
    let lastName = await this.txtLastName.getAttribute(this.attributeValue);
    return lastName;
  }

  /**
   * Get entered username
   * @returns {Promise<string>} inputted username
   * @memberof CreateTenantPage
   */
  public async getEnteredUsername(): Promise<string> {
    let username = await this.txtUsername.getAttribute(this.attributeValue);
    return username;
  }

  /**
   * Get entered email address
   * @returns {Promise<string>} inputted email address
   * @memberof CreateTenantPage
   */
  public async getEnteredEmailAddress(): Promise<string> {
    let emailAddress = await this.txtEmailAddress.getAttribute(this.attributeValue);
    return emailAddress;
  }

  /**
   * Get entered password
   * @returns {Promise<string>} inputted password
   * @memberof CreateTenantPage
   */
  public async getEnteredPassword(): Promise<string> {
    let password = await this.txtPassword.getAttribute(this.attributeValue);
    return password;
  }

  /**
   * Get entered confirm password
   * @returns {Promise<string>} inputted confirmPassword
   * @memberof CreateTenantPage
   */
  public async getEnteredConfirmPassword(): Promise<string> {
    let confirmPassword = await this.txtConfirmPassword.getAttribute(this.attributeValue);
    return confirmPassword;
  }

  /**
   * Create and active tenant by clicking "Create & Activate" button
   * @returns {Promise<TenantPage>} "Tenant" page
   * @memberof CreateTenantPage
   */
  public async createAndActivateTenant(): Promise<TenantPage> {
    await Logger.write(FunctionType.UI, "Creating & Activating Tenant");
    await this.btnCreateActive.click();
    await this.rowSpinner.waitUntilDisappear();
    return TenantPage.getInstance();
  }

  /**
   * Check "New Application" page is displayed
   * @param {string} application name of the new application
   * @returns {Promise<boolean>} Display=>true, not displayed=>false
   * @memberof CreateTenantPage
   */
  public async isNewApplicationDisplayed(application: string): Promise<boolean> {
    await this.btnNext.waitUntilDisappear();
    return await this.getApplication(application).isDisplayed();
  }
}