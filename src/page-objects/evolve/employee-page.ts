import TopMenu from "./top-menu";
import ElementWrapper from "../../utilities/protractor-wrappers/element-wrapper";
import { by } from "protractor";
import { AgentRole } from "../../data-objects/agent";
import { Logger, LogType, FunctionType } from "../../utilities/logger";
import LoginPage from "../../page-objects/evolve/login-page";
import { Mailinator } from "../mailbox/mailinator";
import SetPasswordPage from "../../page-objects/evolve/set-password-page";
import { evolveClusters } from "../../test-data/evolve-cluster-info";
import { EvolveClusters, ClusterID } from "../../data-objects/cluster";
import { JsonObject, JsonProperty } from "json2typescript";
import { JsonConvert, OperationMode, ValueCheckingMode } from "json2typescript";
import { Agent, AgentType } from "../../data-objects/agent";
import BrowserWrapper from "../../utilities/protractor-wrappers/browser-wrapper";

export default class EmployeesPage extends TopMenu {

	constructor() { super(); }

	private static _employeesPage: EmployeesPage = null;

	public static async getInstance(): Promise<EmployeesPage> {
		// this._employeesPage = (this._employeesPage == null) ? new EmployeesPage() : this._employeesPage;
		this._employeesPage = new EmployeesPage();
		await this._employeesPage.waitForPageLoad();
		return this._employeesPage;
	}

	protected btnNewEmployee = new ElementWrapper(by.xpath("//button[@id='newUser']"));
	protected searchEmployee = new ElementWrapper(by.xpath('//input[@ng-model="$parent.searchCriteria"]'));
	protected successMessage = new ElementWrapper(by.xpath('//div[@class="toast-message" and text()="Employee was created successfully!"]'));
	protected deleteMessage = new ElementWrapper(by.xpath('//div[@class="toast-message" and text()="Employee was deleted successfully. Future schedules of this employee have been deleted."]'));
	protected deleteButton = new ElementWrapper(by.xpath('//button[@class="btn btn-sm pull-right ng-binding ng-scope delete-btn btn-secondary"]'));
	protected yesButton = new ElementWrapper(by.xpath('//button[@id="popup-delete"]'));
	protected divEmployeeContainer = new ElementWrapper(by.xpath("//div[@class='ag-body-container']"));
	protected createNewEmployeeForm = CreateNewEmployeeForm.getInstance();
	protected rowSpinner = new ElementWrapper(by.xpath('//div[@class="row wfmspinner"]'));

	/**
	 * Format dynamic control
	 * @param {String} email 
	 * @returns new xpath
	 */

	protected getBtnInviteEmployee(email: string): ElementWrapper {
		return new ElementWrapper(by.xpath(`//div[@colid='emailAddress'][.//span[text()='${email}']]/following-sibling::div[@colid='status'][.//button[contains(@class,'btn-invite')]]`));
	}

	protected getDeleteIconButton(email: string): ElementWrapper {
		return new ElementWrapper(by.xpath(`//div[@colid='emailAddress'][.//span[text()='${email}']]/following-sibling::div[@colid='deleteButton']/button`));
	}

	/**
	* Check New Employee is created in "Employee" page
	* @returns {Promise<boolean>} Display=>true, not displayed=>false
	* @memberof EmployeesPage
	*/
	public async isNewEmployeeCreated(email: string): Promise<boolean> {
		return await this.getBtnInviteEmployee(email).isDisplayed();
	}

	/**
	 * Check "Agent Option" field is displayed in "Employee" page
	 * @returns {Promise<boolean>} Display=>true, not displayed=>false
	 * @memberof EmployeesPage
	 */
	public async isPageDisplayed(): Promise<boolean> {
		return await this.cbxAgentOptions.isDisplayed();
	}

	/**
	 * Is "Success" Messages displayed in "Employee" page
	 * @returns {Promise<boolean>} Display=>true, not displayed=>false
	 * @memberof EmployeesPage
	 */
	public async isSuccessMessageDisplayed(): Promise<boolean> {
		return await this.successMessage.isDisplayed();
	}

	/**
	 * Is "Delete" Message displayed in "Employee" page
     * @returns {Promise<boolean>} Display=>true, not displayed=>false
     * @memberof EmployeesPage
    */
	public async isDeleteMessageDisplayed(): Promise<boolean> {
		return await this.deleteMessage.isDisplayed();
	}

	/**
	 * Click "Invite" button in "Employee" list.
     * @param {any} employee information of employee to invite
     * @returns {Promise<EmployeesPage>} "Employee" page
     * @memberof EmployeesPage
     */
	public async inviteNewEmployee(employee): Promise<EmployeesPage> {
		await this.searchEmployeeName(employee.firstname + " " + employee.lastname);
		await this.getBtnInviteEmployee(employee.emailaddress).click();
		return this;
	}

	/**
	 * Click "New Employee" button to open "New Employee" form
	 * @returns {Promise<EmployeesPage>} "Create new employee" form
	 * @memberof EmployeesPage
	 */
	public async openNewEmployeeForm(): Promise<EmployeesPage> {
		await this.btnNewEmployee.click();
		await this.createNewEmployeeForm.waitForCreateNewEmployeeForm();
		await this.rowSpinner.waitUntilDisappear();
		return this;
	}

	/**
	 * Search employee using employee's name
	 * @param {string} employeeName name of employee to search 
	 * @returns {Promise<EmployeesPage>} "Employee" page
	 * @memberof EmployeesPage
	 */
	public async searchEmployeeName(employeeName: string): Promise<EmployeesPage> {
		await Logger.write(FunctionType.UI, "Searching Employee");
		await this.searchEmployee.type(employeeName);
		await this.divEmployeeContainer.waitUntilPropertyChange("height", 5);
		return this;
	}

	/**
	 * Delete employee in "Employee" Page
	 * @param {string} email email of employee to delete
	 * @returns {Promise<EmployeesPage>} "Employee" page
	 * @memberof EmployeesPage
	 */
	public async deleteEmployee(email: string): Promise<EmployeesPage> {
		await Logger.write(FunctionType.UI, "Deleting Employee");
		await this.getDeleteIconButton(email).click();
		await this.yesButton.click();
		return this;
	}

	/**
	 * Activate New Employee 
	 * @param {string} emailUrl Email URL used to activate New Employee
	 * @returns {SetPasswordPage} Return to SetPasswordPage
	 * @memberof EmployeesPage
	 */
	public async activateNewEmployee(emailUrl: string): Promise<SetPasswordPage> {
		await Logger.write(FunctionType.UI, "Activating Employee");
		await BrowserWrapper.get(emailUrl);
		return SetPasswordPage.getInstance();
	}

	/**
	 * Is "Create New Employee" form displayed ?
	 * @returns {Promise<boolean>} Display=>true, not displayed=>false
	 * @memberof EmployeesPage
	 */
	public async isCreateNewEmployeeFormDisplayed(): Promise<boolean> {
		return await this.createNewEmployeeForm.isCreateNewEmployeeFormDisplayed();
	}

	/**
	 * Select the role of new employee
	 * @param {AgentRole} role role of new employee
	 * @returns {Promise<EmployeesPage>} "Create new employee" form
	 * @memberof EmployeesPage
	 */
	public async selectRole(role: AgentRole): Promise<EmployeesPage> {
		await this.createNewEmployeeForm.selectRole(role);
		return this;
	}

	/**
	 * Fill information of new employee
	 * @param {Agent} employee information of employee to create
	 * @returns {Promise<EmployeesPage>} "Create New Employee" Form
	 * @memberof EmployeesPage
	 */
	public async fillNewEmployeeForm(employee: Agent): Promise<EmployeesPage> {
		await this.createNewEmployeeForm.fillNewEmployeeForm(employee);
		return this;
	}

	/**
	 * Create new employee
	 * @param {Agent} employee name of new employee
	 * @returns {Promise<EmployeesPage>} "Create New Employee" Form
	 * @memberof EmployeesPage
	 */
	public async createNewEmployee(employee: Agent): Promise<EmployeesPage> {
		await this.createNewEmployeeForm.createNewEmployee(employee);
		return this;
	}

	/**
	 * Delete employee using Delete icon button in "Employee" list.
	 * @returns {Promise<EmployeesPage>} "Employee" page
	 * @memberof EmployeesPage
	 */
	public async clickSaveButton(): Promise<EmployeesPage> {
		return await this.createNewEmployeeForm.clickSaveButton();
	}

	/**
	 * Check employee created info
	 * @returns {Promise<boolean>} "Employee" page
	 * @memberof EmployeesPage
	 * */
	public async isEmployeeInfoCorrect(employee: Agent): Promise<boolean> {
		return this.createNewEmployeeForm.isEmployeeInfoCorrect(employee);
	}

	/**
	 * Wait for Create New Employee Form
	 * @returns {Promise<ElementWrapper>}
	 * @memberof CreateNewEmployeeForm
	 */
	public async waitForCreateNewEmployeeForm(): Promise<ElementWrapper> {
		return await this.createNewEmployeeForm.waitForCreateNewEmployeeForm();
	}
}

class CreateNewEmployeeForm {
	private static _createNewEmployeeForm: CreateNewEmployeeForm = null;

	public static getInstance(): CreateNewEmployeeForm {
		this._createNewEmployeeForm = new CreateNewEmployeeForm();
		// this._createNewEmployeeForm = (this._createNewEmployeeForm == null) ? new CreateNewEmployeeForm() : this._createNewEmployeeForm;
		return this._createNewEmployeeForm;
	}

	protected txtFirstName = new ElementWrapper(by.xpath("//input[@id='firstName']"));
	protected txtLastName = new ElementWrapper(by.xpath("//input[@id='lastName']"));
	protected btnRoleSelection = new ElementWrapper(by.xpath("//div[@id='user_roles']"));
	protected txtEmailAddress = new ElementWrapper(by.xpath("//input[@id='emailAddress']"));
	protected txtUsername = new ElementWrapper(by.xpath("//input[@id='username']"));
	protected btnSave = new ElementWrapper(by.xpath("//button[@id='saveWithPopoverBtn']"));
	protected btnCancel = new ElementWrapper(by.xpath("//button[@id='cancel']"));
	protected createEmployeeForm = new ElementWrapper(by.xpath("//div[@ng-form='userForm']"));
	protected rowSpinner = new ElementWrapper(by.xpath('//div[@class="row wfmspinner"]'));

	protected getItemRoleSelection(role: AgentRole): ElementWrapper {
		return new ElementWrapper(by.xpath(`//div[@class="ui-select-choices ui-select-dropdown selectize-dropdown ng-scope single"]//span[text()='${role}']`));
	}

	/**
	 * Is "Create New Employee" form displayed ?
	 * @returns {Promise<boolean>} Display=>true, not displayed=>false
	 * @memberof CreateNewEmployeeForm
	 */
	public async isCreateNewEmployeeFormDisplayed(): Promise<boolean> {
		return await this.createEmployeeForm.isDisplayed();
	}

	/**
	 * Select the role of new employee
	 * @param {AgentRole} role role of new employee
	 * @returns {Promise<CreateNewEmployeeForm>} "Create new employee" form
	 * @memberof CreateNewEmployeeForm
	 */
	public async selectRole(role: AgentRole): Promise<CreateNewEmployeeForm> {
		await this.btnRoleSelection.click();
		await this.getItemRoleSelection(role).click();
		return this;
	}

	/**
	 * Fill information of new employee
	 * @param {Agent} employee information of employee to create
	 * @returns {Promise<CreateNewEmployeeForm>} "Create New Employee" Form
	 * @memberof CreateNewEmployeeForm
	 */
	public async fillNewEmployeeForm(employee: Agent): Promise<CreateNewEmployeeForm> {
		await this.txtFirstName.type(employee.firstname);
		await this.txtLastName.type(employee.lastname);
		await this.selectRole(employee.role);
		await this.txtEmailAddress.type(employee.email);
		await this.rowSpinner.waitUntilDisappear();
		return this;
	}

	/**
	 * Create new employee
	 * @param {Agent} employee name of new employee
	 * @returns {Promise<CreateNewEmployeeForm>} "Create New Employee" Form
	 * @memberof CreateNewEmployeeForm
	 */
	public async createNewEmployee(employee: Agent): Promise<CreateNewEmployeeForm> {
		await this.fillNewEmployeeForm(employee);
		await this.btnSave.click();
		await this.createEmployeeForm.waitUntilDisappear();
		return this;
	}

	/**
	 * Delete employee using Delete icon button in "Employee" list.
	 * @returns {Promise<EmployeesPage>} "Employee" page
	 * @memberof CreateNewEmployeeForm
	 */
	public async clickSaveButton(): Promise<EmployeesPage> {
		await Logger.write(FunctionType.UI, "Clicking Save Button on Create Employee Form");
		await this.btnSave.click();
		return EmployeesPage.getInstance();
	}

	/**
	 * Is employee created info displayed correctly.
	 * @returns {Promise<boolean>} "Employee" page
	 * @memberof CreateNewEmployeeForm
	 */
	public async isEmployeeInfoCorrect(employee: Agent): Promise<boolean> {
		let firstNameString: string = await this.txtFirstName.getAttribute("value");
		let lastNameString: string = await this.txtLastName.getAttribute("value");
		let roleString: string = await this.btnRoleSelection.getText();
		let emailAddressString: string = await this.txtEmailAddress.getAttribute("value");
		let userNameString: string = await this.txtUsername.getAttribute("value");

		if (firstNameString == employee.firstname && lastNameString == employee.lastname
			&& roleString == employee.role && emailAddressString == employee.email
			&& userNameString == employee.email) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Wait for Create New Employee Form
	 * @returns {Promise<ElementWrapper>}
	 * @memberof CreateNewEmployeeForm
	 */
	public async waitForCreateNewEmployeeForm(): Promise<ElementWrapper> {
		return await this.createEmployeeForm.waitUntilPropertyNotChange("value", 5);
	}
}
