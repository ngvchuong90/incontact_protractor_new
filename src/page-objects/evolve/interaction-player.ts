import { by } from "protractor";
import { Logger, LogType, FunctionType } from "../../utilities/logger";
import ElementWrapper from "../../utilities/protractor-wrappers/element-wrapper";
import TopMenu from "./top-menu";
import BrowserWrapper from "../../utilities/protractor-wrappers/browser-wrapper";

export default class InteractionPlayer extends TopMenu {

	constructor() { super(); }

	private static _interactionPlayer: InteractionPlayer = null;

	public static async getInstance(): Promise<InteractionPlayer> {
		// this._interactionPlayer = (this._interactionPlayer == null) ? new InteractionPlayer() : this._interactionPlayer;
		this._interactionPlayer = new InteractionPlayer();
		await BrowserWrapper.switchWindow(1);
		await this._interactionPlayer.waitForLoading();
		return this._interactionPlayer;
	}

	protected spanAgentName = new ElementWrapper(by.xpath("(//div[@class='participantsSideBar']//span[contains(@class, 'participantName ')])[1]"));
	protected spanCustomerName = new ElementWrapper(by.xpath("(//div[@class='participantsSideBar']//span[contains(@class, 'participantName ')])[2]"));
	protected spanStartTime = new ElementWrapper(by.xpath("//span[@id='recordingStartTime']"));
	protected spanEndTime = new ElementWrapper(by.xpath("//span[@id='recordingEndTime']"));
	protected spanCurrentTotalTime = new ElementWrapper(by.xpath("//span[@id='currentTimeTotalTime']"));
	protected divTimeline = new ElementWrapper(by.xpath("//div[@id='timelineCanvas']"));

	/**
	 * Check info agent is displayed correctly
	 * @param {string} agentName name of the agent to check 
	 * @param {string} customerName name of the customer to check 
	 * @returns {Promise<boolean>} Display=>true, not displayed=>false
	 * @memberof InteractionPlayer
	 */
	public async isInfoDisplayedCorrectly(agentName: string, customerName: string): Promise<boolean> {
		let isAgentDisplayed = await this.isAgentDisplayed(agentName);
		let isCustomerDisplayed = await this.isCustomerDisplayed(customerName);
		let isDirectionDisplayed = await this.isDirectionDisplayed();
		let isRecordedTimeDisplayed = await this.isRecordedTimeDisplayed();
		return (true == isAgentDisplayed == isCustomerDisplayed == isDirectionDisplayed == isRecordedTimeDisplayed);
	}

	/**
	 * Check agent is displayed
	 * @param {string} agentName name of the agent to check 
	 * @returns {Promise<boolean>} Display=>true, not displayed=>false
	 * @memberof InteractionPlayer
	 */
	public async isAgentDisplayed(agentName: string): Promise<boolean> {
		let actualAgent = await this.spanAgentName.getText();
		return (actualAgent == agentName);
	}

	/**
	 * Check customer is displayed
	 * @param {string} customerName name of the customer to check 
	 * @returns {Promise<boolean>} Display=>true, not displayed=>false
	 * @memberof InteractionPlayer
	 */
	public async isCustomerDisplayed(customerName: string): Promise<boolean> {
		let actualCus = await this.spanCustomerName.getText();
		return (actualCus == customerName);
	}

	/**
	 * Check direction is displayed
	 * @returns {Promise<boolean>} Display=>true, not displayed=>false
	 * @memberof InteractionPlayer
	 */
	public async isDirectionDisplayed(): Promise<boolean> {
		return await this.divTimeline.isDisplayed();
	}

	/**
	 * Check recorded time is displayed
	 * @returns {Promise<boolean>} Display=>true, not displayed=>false
	 * @memberof InteractionPlayer
	 */
	public async isRecordedTimeDisplayed(): Promise<boolean> {
		let endDate = await this.getEndTime();
		let expectedDate = await this.getExpectedEndTime();

		//we have +-1 as tolerance for the end-date
		if (endDate.getTime() === expectedDate.getTime()) {
			return true;
		} else if (endDate.getTime() === expectedDate.setSeconds(expectedDate.getSeconds() + 1)) {
			return true;
		}
		else if (endDate.getTime() === expectedDate.setSeconds(expectedDate.getSeconds() - 1)) {
			return true;
		}

		return false;
	}

	/**
	 * Get expected end time of player
	 * @returns {Promise<Date>} expected end time
	 * @memberof InteractionPlayer
	 */
	public async getExpectedEndTime(): Promise<Date> {
		let startTime = await this.getStartTime();
		let duration = await this.getDuration();
		let expectedEndTime = new Date(startTime);
		expectedEndTime.setSeconds(startTime.getSeconds() + duration);
		return expectedEndTime;
	}

	/**
	 * Get start time of player
	 * @returns {Promise<Date>} start time
	 * @memberof InteractionPlayer
	 */
	public async getStartTime(): Promise<Date> {
		let value = await this.spanStartTime.getText();
		return new Date(value);
	}

	/**
	 * Get end time of player
	 * @returns {Promise<Date>} end time
	 * @memberof InteractionPlayer
	 */
	public async getEndTime(): Promise<Date> {
		let value = await this.spanEndTime.getText();
		return new Date(value);
	}

	/**
	 * Get duration of player
	 * @returns {Promise<number>} duration
	 * @memberof InteractionPlayer
	 */
	public async getDuration(): Promise<number> {
		let totalTime = await this.spanCurrentTotalTime.getText();
		let value = totalTime.split("/ ")[1];
		let totalSeconds = (parseInt(value.split(":")[0]) * 60) + parseInt(value.split(":")[1]);
		return totalSeconds;
	}

	/**
	 * Close Interaction player
	 * @returns {Promise<void>} the interaction player is closed
	 * @memberof InteractionPlayer
	 */
	public async close(): Promise<void> {
		await Logger.write(FunctionType.UI, "Closing Interaction Player");
		await BrowserWrapper.close();
		await BrowserWrapper.switchWindow(0);
	}

	/**
	 * Wait for time line control is displayed
	 * @returns {Promise<InteractionPlayer>} the existence of interaction player
	 * @memberof InteractionPlayer
	 */
	public async waitForLoading(): Promise<InteractionPlayer> {		
		await this.divTimeline.isDisplayed();
		return this;
	}

}
