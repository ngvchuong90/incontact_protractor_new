import { by } from "protractor";
import ElementWrapper from "../../utilities/protractor-wrappers/element-wrapper";
import { Logger, LogType, FunctionType } from '../../utilities/logger';
import { Agent } from "../../data-objects/agent";
import EmployeesPage from './employee-page';
import TenantPage from "./tenant-page";

export default class LoginPage {

	private static _loginPage: LoginPage = null;

	protected txtUsername = new ElementWrapper(by.xpath("//input[@id='emailField']"));
	protected txtPassword = new ElementWrapper(by.xpath("//input[@id='passField']"));
	protected btnLogin = new ElementWrapper(by.xpath("//button[@id='loginBtn']"));
	protected requireMsg = new ElementWrapper(by.xpath("//div[@ng-message='required']"));
	protected emailMsg = new ElementWrapper(by.xpath("//div[@ng-message='emailCharacters']"));

	public static getInstance(): LoginPage {
		// this._loginPage = (this._loginPage == null) ? new LoginPage() : this._loginPage;
		this._loginPage = new LoginPage();
		return this._loginPage;
	}

	/**
	 * Submit login form
	 * @param {Agent} agent information of the agent
	 * @returns {Promise<void>} 
	 * @memberof LoginPage
	 */
	public async submitLoginForm(agent: Agent): Promise<void> {
		await this.txtUsername.type(agent.email);
		await this.txtPassword.type(agent.password);
		await this.txtPassword.waitUntilPropertyNotChange("value",1);
		await this.btnLogin.click();
	}

	/**
	 * Login as admin account
	 * @param {Agent} agent information of the agent
	 * @returns {Promise<EmployeesPage>} "Employee" page
	 * @memberof LoginPage
	 */
	public async loginAsAdmin(agent: Agent): Promise<EmployeesPage> {
		await Logger.write(FunctionType.UI, `Logging in using Admin ${agent.email}`);
		await this.submitLoginForm(agent);
		await this.waitForLoginSuccess();
		return await EmployeesPage.getInstance();
	}

	/**
	 * Login as super admin account
	 * @param {Agent} agent information of the agent
	 * @returns {Promise<TenantPage>} "Tenant" page
	 * @memberof LoginPage
	 */
	public async loginAsSuperAdmin(agent: Agent): Promise<TenantPage> {
		await Logger.write(FunctionType.UI, `Logging in using Super Admin ${agent.email}`);
		await this.submitLoginForm(agent);
		await this.waitForLoginSuccess();
		return TenantPage.getInstance();
	}

	/**
     * Check login page is displayed or not 
	 * @returns {Promise<boolean>} Display=>true, not displayed=>false
	 * @memberof LoginPage
	 */
	public async isPageDisplayed(): Promise<boolean> {
		return await this.btnLogin.isDisplayed();
	}

	public async waitForLoginSuccess(){
		await this.requireMsg.waitUntilDisappear();
		await this.emailMsg.waitUntilDisappear();
	}
}
