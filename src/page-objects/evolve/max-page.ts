import { by } from "protractor";
import ElementWrapper from "../../utilities/protractor-wrappers/element-wrapper";
import BrowserWrapper from "../../utilities/protractor-wrappers/browser-wrapper";
import { Logger, LogType, FunctionType } from '../../utilities/logger';
import { MaxState, MaxConnectOption } from "../../data-objects/cluster";
import EvolveAPIs from "../../apis/evolve-apis";
import { Agent } from "data-objects/agent";
import { APIResponse } from "../../utilities/api-core";
import { StringUtility, JsonUtility } from "../../utilities/utility";

export default class MaxPage {

	private static _maxPage: MaxPage = null;

	protected divSpinner = new ElementWrapper(by.xpath("//div[@id='index-loading-spinner']"));
	protected divMaxWrapper = new ElementWrapper(by.xpath("//div[@id='uimanager_container']"));
	protected btnStateSection = new ElementWrapper(by.xpath("//div[@id='agentStateSection']//div[@class='state-body']//span[@class='current-state']"));
	protected btnConfirmLogoutMAX = new ElementWrapper(by.xpath("//button[@class='confirm-button' and text()='Log out']"));
	protected radPhone = new ElementWrapper(by.xpath("//input[@id='radioPhone']/.."));
	protected radStation = new ElementWrapper(by.xpath("//input[@id='radioStation']/.."));
	protected radSoftPhone = new ElementWrapper(by.xpath("//input[@id='radioSoftPhone']/.."));
	protected txtPhone = new ElementWrapper(by.xpath("//input[@id='phoneNumberText']"));
	protected txtStation = new ElementWrapper(by.xpath("//input[@id='stationIdText']"));
	protected cbxRememberMe = new ElementWrapper(by.xpath("//input[@id='cbRememberMe']"));
	protected btnConnect = new ElementWrapper(by.xpath("//div[@class='select-options']/button[@class='button connect']/h4"));
	protected btnClose = new ElementWrapper(by.xpath("//div[@class='select-options']/button[@class='button close']/h4"));
	protected divStateBar = new ElementWrapper(by.xpath("//div[@class='state-bar']"));

	// Toolbar Section Item
	protected newToolbarButton = new ElementWrapper(by.xpath("//li[@class='toolbar-button-template']//div[@class='button-text' and contains(text(),'New')]"));

	protected getToolbarButton(name: string): ElementWrapper {
		return new ElementWrapper(by.xpath(`//li[@class='toolbar-button-template']//div[@class='button-text' and contains(text(),'${name}')]`));
	}

	// Add New Popver
	protected addNewPopover = new ElementWrapper(by.xpath("//header[contains(@class,'new-media-panel') and @title='Add New']"));
	protected newOutboundCallMediaList = new ElementWrapper(by.xpath("//ul[@class='new-media-list']//span[@title='Outbound Call']"));

	// Advanced address book
	protected searchTextBox = new ElementWrapper(by.xpath("//div[@id='advancedAddressBookSection']//div[@class='search-section']//input[@type='search']"));
	protected advancedAddressBook = new ElementWrapper(by.xpath("//div[@id='advancedAddressBookSection']//div[@class='popover-panel advanced-address-book-ui']"));
	protected callButtonSearchResultList = new ElementWrapper(by.xpath("//ul[@class='result-list']//button[@class='call']"));
	protected callSkillList = new ElementWrapper(by.xpath("//div[@class='selectmenu-button skill-list']"));
	protected selectSkillList = new ElementWrapper(by.xpath("//select[@class='skills-dropdown']"))

	protected getSelectSkillList(phoneNumber: string): ElementWrapper {
		return new ElementWrapper(by.xpath(`//ul[@class='skills-dropdown']//div[./h1[text()='${phoneNumber}']]//select[@class='skill-list']`));
	}

	protected getCallSkill(skillName: string): ElementWrapper {
		return new ElementWrapper(by.xpath(`//div[contains(@class,'skill-list-container')]//li[@class='selectmenu-menu-item'][@title='${skillName}']`));
	}

	protected getSearchValueResult(value: string): ElementWrapper {
		return new ElementWrapper(by.xpath(`//ul[@class='result-list']//h1[contains(text(),'${value}')]`));
	}

	// Call workspace
	protected divCallWorkspace = new ElementWrapper(by.xpath("//div[@id='contactSection']"));
	protected btnHangUp = new ElementWrapper(by.xpath("//button[@class='end-contact']"));
	protected btnConfirmHangUp = new ElementWrapper(by.xpath("//button[@class='confirm-end-contact']"));
	protected lblDialling = new ElementWrapper(by.xpath("//div[@class='contact-info']//span[@class='state-name'][text()='Dialing']"));
	protected btnRecord = new ElementWrapper(by.xpath("//button[@class='record' or @class='record disabled']"));

	protected getMaxStateItem(maxState: MaxState): ElementWrapper {
		return new ElementWrapper(by.xpath(`//li[@data-state='${maxState}' and not(@class ='state-item-template')]`));
	}

	public static async getInstance(): Promise<MaxPage> {
		// this._maxPage = (this._maxPage == null) ? new MaxPage() : this._maxPage;
		this._maxPage = new MaxPage();
		await BrowserWrapper.switchWindow(1);
		await this._maxPage.waitForLoading();
		return this._maxPage;
	}

	/**
	 * Open MAX popover by clicking toolbar button
	 * @param {string} name Name of MAX pop over
	 * @returns {Promise<void>} 
	 * @memberof MaxPage
	 */
	public async openMaxPopOver(name: string): Promise<void> {
		await Logger.write(FunctionType.UI, "Opening MAX Popover");
		await this.getToolbarButton(name).click();
	}

	/**
	 * Search address book
	 * @param {string} value value to search
	 * @returns {Promise<void>} 
	 * @memberof MaxPage
	 */
	public async searchAddressBook(value: string): Promise<void> {
		await Logger.write(FunctionType.UI, "Searching address book");
		await this.searchTextBox.type(value);
	}

	/**
	 * Click search result button
	 * @param {string} value click search result button based on value
	 * @returns {Promise<void>} 
	 * @memberof MaxPage
	 */
	public async clickSearchResult(value: string): Promise<void> {
		await Logger.write(FunctionType.UI, "Clicking Search Result");
		await this.getSearchValueResult(value).click();
	}

	/**
	 * Check if Agent has multi OB skills
	 * @param {Agent} agent agent name
	 * @returns {Promise<boolean>} the existence of multi OB skills
	 * @memberof MaxPage
	 */
	public async doesAgentHaveMultiOBSkills(agent: Agent): Promise<boolean> {
		await Logger.write(FunctionType.API, "Checking if Agent Has Multi Outbound Skills");

		let skillCount: number = 0;
		let stringAgentSkill: APIResponse = await EvolveAPIs.getAgentSkills(agent);
		let fieldCount: number = JsonUtility.getFieldCount(stringAgentSkill.body, "resultSet.agentSkillAssignments");
		for (let i = 0; i < fieldCount; i++) {
			let isOutbound: string = JsonUtility.getFieldValue(stringAgentSkill.body, `resultSet.agentSkillAssignments[${i}].isOutbound`);
			let mediaTypeName: string = JsonUtility.getFieldValue(stringAgentSkill.body, `resultSet.agentSkillAssignments[${i}].mediaTypeName`);
			if (isOutbound == "\"True\"" && mediaTypeName == "\"Phone Call\"") {
				skillCount += 1;
			}
		}

		if (skillCount > 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Call phone number
	 * @param {string} phoneNumber phone number to call out
	 * @param {string} skillName skill name
	 * @returns {Promise<void>} 
	 * @memberof MaxPage
	 */
	public async callPhoneNumber(agent: Agent, phoneNumber: string, skillName: string): Promise<void> {
		await Logger.write(FunctionType.UI, `Calling phone number ${phoneNumber}`);

		await this.openMaxPopOver("New");
		await this.searchAddressBook(phoneNumber);
		await this.callButtonSearchResultList.waitUntilPropertyNotChange("value", 2);
		await this.callButtonSearchResultList.click();

		if (await this.doesAgentHaveMultiOBSkills(agent) == true) {
			await this.getCallSkill(skillName).waitUntilPropertyNotChange("value", 2);
			await this.getCallSkill(skillName).click();
		}

		await this.callButtonSearchResultList.waitUntilDisappear(5);
		await this.divCallWorkspace.wait();
		await this.waitForCallDialling();
	}

	/**
	 * Wait call workspace to display
	 * @param {boolean} state state of agent
	 * @returns {Promise<void>} 
	 * @memberof MaxPage
	 */
	// public async waitForCallWorkSpaceDisplay(): Promise<void> {
	// 		await this.divCallWorkspace.wait();
	// }

	/**
	 * Check MAX page is displayed or not
	 * @returns {Promise<boolean>} the existance of MAX page
	 * @memberof MaxPage
	 */
	public async isPageDisplayed(): Promise<boolean> {
		return await this.btnStateSection.isDisplayed();
	}

	/**
	 * Change MAX state
	 * @param {MaxState} maxState State of agent
	 * @returns {Promise<MaxPage>} 
	 * @memberof MaxPage
	 */
	public async changeState(maxState: MaxState): Promise<MaxPage> {
		await Logger.write(FunctionType.UI, `Changing Max status to ${maxState}`);
		await this.btnStateSection.click();
		await this.getMaxStateItem(maxState).click();
		await this.getMaxStateItem(maxState).waitUntilDisappear();
		return this;
	}

	/**
	 * Log out MAX
	 * @returns {Promise<void>} 
	 * @memberof MaxPage
	 */
	public async logOut(): Promise<void> {
		await Logger.write(FunctionType.UI, "Logging out from MAX");
		await this.changeState(MaxState.LOGOUT);
		await this.btnConfirmLogoutMAX.click();
		await BrowserWrapper.waitForNumberOfWindows(1);
		await BrowserWrapper.switchWindow(0);
	}

	/**
	 * Wait for loading
	 * @returns {Promise<MaxPage>} MAX page
	 * @memberof MaxPage
	 */
	public async waitForLoading(): Promise<MaxPage> {
		await this.divSpinner.waitUntilDisappear();
		await this.divMaxWrapper.isDisplayed();
		return this;
	}

	/**
	 * Enter launch MAX form
	 * @param {MaxConnectOption} phoneType Phone Type value
	 * @param {string} phoneNumber Phone number value
	 * @param {boolean} [rememberMe=false] Select Remember Me checkbox, default value is false
	 * @returns {Promise<MaxPage>} 
	 * @memberof MaxPage
	 */
	public async enterLaunchForm(phoneType: MaxConnectOption, phoneNumber: string, rememberMe: boolean = false): Promise<MaxPage> {
		if (phoneType == MaxConnectOption.SOFTPHONE) {
			await this.radSoftPhone.click();
		} else {
			await Logger.write(FunctionType.UI, "Entering information to the Launching Max form");

			if (phoneType == MaxConnectOption.PHONE) {
				await this.radPhone.click();
				await this.txtPhone.type(phoneNumber);
			}
			else if (phoneType == MaxConnectOption.STATIONID) {
				await this.radStation.click();
				await this.txtStation.type(phoneNumber);
			}
		}

		if (rememberMe) {
			await this.cbxRememberMe.click();
		}

		return await this;
	}

	/**
	 * Submit MAX launch form
	 * @param {MaxConnectOption} phoneType Phone Type value
	 * @param {string} phoneNumber Phone number value
	 * @param {boolean} [rememberMe=false] Select Remember Me checkbox, default value is false
	 * @returns {Promise<MaxPage>} 
	 * @memberof MaxPage
	 */
	public async submitLaunchForm(phoneType: MaxConnectOption, phoneNumber: string, rememberMe: boolean = false): Promise<MaxPage> {
		await this.enterLaunchForm(phoneType, phoneNumber, rememberMe);
		await this.btnConnect.click();

		return this;
	}

	/**
	 * Connect MAX
	 * @returns {Promise<MaxPage>} MAX page
	 * @memberof MaxPage
	 */
	public async connectMax(): Promise<MaxPage> {
		await Logger.write(FunctionType.UI, "Connecting MAX");
		await this.btnConnect.click();
		await this.newToolbarButton.wait();
		return this;
	}

	/**
	 * Get entered phone number
	 * @param {MaxConnectOption} phoneType Phone Type value
	 * @returns {Promise<string>} phone number 
	 * @memberof MaxPage
	 */
	public async getEnteredPhone(phoneType: MaxConnectOption): Promise<string> {
		let result: string = "";
		if (phoneType == MaxConnectOption.PHONE) {
			let result = await BrowserWrapper.executeScript("return document.getElementById('phoneNumberText').value;");
		}
		else if (phoneType == MaxConnectOption.STATIONID) {
			let result = await BrowserWrapper.executeScript("return document.getElementById('stationIdText').value;");
		}
		return result;
	}

	/**
	 * Check MAX is launched or not 
	 * @returns {Promise<boolean>} the existence of MAX
	 * @memberof MaxPage
	 */
	public async isMaxLauched(): Promise<boolean> {
		return await this.btnStateSection.isDisplayed();
	}

	/**
	 * Get agent status
	 * @returns {Promise<string>} Agent state
	 * @memberof MaxPage
	 */
	public async getAgentStatus(): Promise<string> {
		return await this.btnStateSection.getText();
	}

	/**
	 * Check the call is accepted or not 
	 * @returns {Promise<boolean>} existence of Workspace and Hang Up button
	 * @memberof MaxPage
	 */
	public async isCallAccepted(): Promise<boolean> {
		let isWorkSpaceDisplayed = await this.divCallWorkspace.isDisplayed();

		if (isWorkSpaceDisplayed) {
			return await this.btnHangUp.isDisplayed();
		}

		return false;
	}

	/**
	 * End call
	 * @returns {Promise<MaxPage>} MAX page
	 * @memberof MaxPage
	 */
	public async endCall(): Promise<MaxPage> {
		await Logger.write(FunctionType.UI, "Ending call");
		await this.btnHangUp.click();
		await this.btnConfirmHangUp.click();
		await this.btnConfirmHangUp.waitUntilDisappear();
		return this;
	}

	/**
	 * Check the call is ended or not
	 * @returns {Promise<boolean>} the Agent state
	 * @memberof MaxPage
	 */
	public async isCallEnded(): Promise<boolean> {
		await this.btnStateSection.waitUntilPropertyNotChange("title", 2);
		return (await this.getAgentStatus() == MaxState.AVAILABLE.toUpperCase());
	}

	/**
	 * Wating for call dialling
	 * @returns {Promise<void>} the Agent state
	 * @memberof MaxPage
	 */
	public async waitForCallDialling(): Promise<void> {
		await Logger.write(FunctionType.UI, "Wating for call dialling");
		await this.lblDialling.waitUntilDisappear(15);
		await this.btnRecord.waitUntilPropertyNotChange("value", 10);
	}

}