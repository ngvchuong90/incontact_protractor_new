import TopMenu from './top-menu';

export default class MySchedulePage extends TopMenu {

	private static _mySchedulePage: MySchedulePage = null;

	private constructor() { super(); }

	public static getInstance(): MySchedulePage {
		// this._mySchedulePage = (this._mySchedulePage == null) ? new MySchedulePage() : this._mySchedulePage;
		this._mySchedulePage = new MySchedulePage();
		return this._mySchedulePage;
	}
}