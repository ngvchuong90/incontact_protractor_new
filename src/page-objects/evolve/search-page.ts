import { by } from "protractor";
import ElementWrapper from '../../utilities/protractor-wrappers/element-wrapper';
import TopMenu from '../evolve/top-menu';
import { Logger, LogType, FunctionType } from '../../utilities/logger';
import InteractionPlayer from "./interaction-player";
import { SearchTimeRange, SearchColumnName } from "../../data-objects/cluster";

export default class SearchPage extends TopMenu {

	private static _searchPage: SearchPage = null;

	protected btnTimeSelection = new ElementWrapper(by.xpath("//button[@id='single-button']"));
	protected txtSearch = new ElementWrapper(by.xpath("//input[@id='search-input']"));
	protected btnSearch = new ElementWrapper(by.xpath("//button[@class='search-box-button btn']"));
	protected lblTotalFound = new ElementWrapper(by.xpath("//div[@class='total-label']"));

	protected getLblSearchResult(rowNumber: number, columnName: string): ElementWrapper {
		return new ElementWrapper(by.xpath(`//div[@class='ag-body-container']/div[${rowNumber}]/div[count(//span[text()='${columnName}']/ancestor::div[@colid]/preceding-sibling::div) + 1]`));
	}

	protected getItemTimeSelection(time: string): ElementWrapper {
		return new ElementWrapper(by.xpath(`//a[text()='${time}']`));
	}

	protected getBtnPlayRecord(index: number): ElementWrapper {
		return new ElementWrapper(by.xpath(`(//span[contains(@class, 'player')])[${index}]`));
	}

	protected constructor() { super(); }

	public static getInstance(): SearchPage {
		// this._searchPage = (this._searchPage == null) ? new SearchPage() : this._searchPage;
		this._searchPage = new SearchPage();
		return this._searchPage;
	}

	/**
	 * Select time
	 * @param {SearchTimeRange} time Time to select
	 * @returns {Promise<void>} 
	 * @memberof SearchPage
	 */
	public async selectTime(time: SearchTimeRange): Promise<void> {
		await this.btnTimeSelection.click();
		await this.getItemTimeSelection(time).click();
	}

	/**
	 * Search item by given time and value
	 * @param {SearchTimeRange} time Time to search
	 * @param {string} value value to search
	 * @returns {Promise<SearchPage>} Search page
	 * @memberof SearchPage
	 */
	public async search(time: SearchTimeRange, value: string): Promise<SearchPage> {
		await Logger.write(FunctionType.UI, "Searching in Search page");
		await this.selectTime(time);
		await this.txtSearch.type(value);
		await this.btnSearch.click();
		return await this.waitForSearch();
	}

	/**
	 * Wait for search 
	 * @returns {Promise<SearchPage>} Search Page
	 * @memberof SearchPage
	 */
	public async waitForSearch(): Promise<SearchPage> {
		await this.lblTotalFound.isDisplayed();
		return this;
	}

	/**
	 * Get search result
	 * @param {number} rowNumber row number value
	 * @param {SearchColumnName} columnName column number value
	 * @returns {Promise<string>}  search result
	 * @memberof SearchPage
	 */
	public async getSearchResult(rowNumber: number, columnName: SearchColumnName): Promise<string> {
		// rowNumber start from 1
		return await this.getLblSearchResult(rowNumber, columnName).getText();
	}

	/**
	 * Check search page is displayed or not 
	 * @returns {Promise<boolean>} the existence of the search page 
	 * @memberof SearchPage
	 */
	public async isPageDisplayed(): Promise<boolean> {
		return await this.btnSearch.isDisplayed();
	}

	/**
	 * Click "Play record" button
	 * @param {number} index index value
	 * @returns {Promise<InteractionPlayer>} Interaction Player page
	 * @memberof SearchPage
	 */
	public async playRecord(index: number): Promise<InteractionPlayer> {
		await Logger.write(FunctionType.UI, `Playing record at index ${index}`);
		await this.getBtnPlayRecord(index).click();
		return await InteractionPlayer.getInstance();
	}
}

