import { by } from "protractor";
import { Logger, LogType, FunctionType } from '../../utilities/logger';
import ElementWrapper from '../../utilities/protractor-wrappers/element-wrapper';
import EmployeesPage from "./employee-page";

export default class SetPasswordPage {

	private static _setPasswordPage: SetPasswordPage = null;

	protected txtPassword = new ElementWrapper(by.xpath("//input[@id='passwordCognito']"));
	protected txtConfirmPassword = new ElementWrapper(by.xpath("//input[@name='confirmPassword']"));
	protected btnResetPassword = new ElementWrapper(by.xpath("//button[@id='createMyAccountBtn']"));

	private constructor() { }

	public static getInstance(): SetPasswordPage {
		// this._setPasswordPage = (this._setPasswordPage == null) ? new SetPasswordPage() : this._setPasswordPage;
		this._setPasswordPage = new SetPasswordPage();
		return this._setPasswordPage;
	}

	/**
	 * Reset password
	 * @param {string} password Password value
	 * @returns {Promise<EmployeesPage>} Employees Page
	 * @memberof SetPasswordPage
	 */
	async resetPassword(password: string): Promise<EmployeesPage> {
		await this.txtPassword.type(password);
		await this.txtConfirmPassword.type(password);
		await this.btnResetPassword.click();
		return EmployeesPage.getInstance();
	}

}

