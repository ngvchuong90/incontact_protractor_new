import { by } from "protractor";
import TopMenu from '../evolve/top-menu';
import { Logger, LogType, FunctionType } from '../../utilities/logger';
import ElementWrapper from '../../utilities/protractor-wrappers/element-wrapper';
import CreateTenantPage from "./create-tenant-page";
import UpdateTenantPage from "./update-tenant-page";

export default class TenantPage extends TopMenu {

    private static _tenantPage: TenantPage = null;

    protected btnNewTenant = new ElementWrapper(by.xpath("//button[@class='btn btn-primary create-tenant-button ng-binding']"));
    protected btnFindTenant = new ElementWrapper(by.xpath("//i[@class='glyphicon glyphicon-search search-icon']"));
    protected txtTenant = new ElementWrapper(by.model("$parent.searchCriteria"));
    protected listTenant = new ElementWrapper(by.xpath("//div[@class='tenant-management-list-grid nice-grid-directive']"));
    protected linkTenants = new ElementWrapper(by.xpath("//a[@id='tenants']"));
    protected linkEmployees = new ElementWrapper(by.xpath("//a[@id='tmUsers']"));
    protected iconActive = new ElementWrapper(by.xpath("(//div[@class='overflow-tooltip-wrapper ng-isolate-scope']//div[@class='status-text ng-scope'])[1]"));
    protected rowSpinner = new ElementWrapper(by.xpath('//div[@class="row wfmspinner"]'));
    protected bodyTable = new ElementWrapper(by.xpath("//div[@class='ag-body-container']"))

    protected searchTenantValue(tenantName: string): ElementWrapper {
        return new ElementWrapper(by.xpath(`//div[@colid='tenantName']//span[text()='${tenantName}']`));
    }

    protected getTenant(tenantName: string): ElementWrapper {
        return new ElementWrapper(by.xpath(`//span[text()='${tenantName}']`));
    }

    constructor() { super() }

    public static async getInstance(): Promise<TenantPage> {
        // this._tenantPage = (this._tenantPage == null) ? new TenantPage() : this._tenantPage;
        this._tenantPage = new TenantPage();
        await this._tenantPage.waitForPageLoad();
        return this._tenantPage;
    }

    /**
     * Create tenant by clicking "New Tenant" button 
     * @returns {Promise<CreateTenantPage>} Create Tenant Page
     * @memberof TenantPage
     */
    public async openCreateTenantPage(): Promise<CreateTenantPage> {
        await Logger.write(FunctionType.UI, "Going to Create Tenant page");
        await this.btnNewTenant.click();
        await this.rowSpinner.waitUntilDisappear();
        return CreateTenantPage.getInstance();
    }

    /**
     * Search and select tenant 
     * @param {string} tenantName Name of tenant
     * @returns {Promise<UpdateTenantPage>} Update Tenant Page
     * @memberof TenantPage
     */
    public async selectTenant(tenantName: string): Promise<UpdateTenantPage> {
        await Logger.write(FunctionType.UI, `Searching ${tenantName} tenant in the list`);
        await this.iconActive.isDisplayed();
        await this.txtTenant.type(tenantName);
        await this.getTenant(tenantName).click();
        await this.rowSpinner.waitUntilDisappear();
        return UpdateTenantPage.getInstance();;
    }

    /**
     * Check tenant page is displayed or not
     * @returns {Promise<boolean>} the existence of tenant page
     * @memberof TenantPage
     */
    public async isPageDisplayed(): Promise<boolean> {
        return await this.btnNewTenant.isDisplayed();
    }

    /**
     * Check new tenant is displayed or not
     * @param {string} tenantName Name of tenant
     * @returns {Promise<boolean>} the existence of new tenant
     * @memberof TenantPage
     */
    public async isNewTenantDisplayed(tenantName: string): Promise<boolean> {
        await this.txtTenant.click();
        await this.txtTenant.type(tenantName);
        return await this.searchTenantValue(tenantName).isDisplayed();
    }
}
