import { by } from "protractor";
import ElementWrapper from "../../utilities/protractor-wrappers/element-wrapper";
import { Logger, LogType, FunctionType } from '../../utilities/logger';
import LoginPage from "./login-page";
import MaxPage from "./max-page";

export default class TopMenu {

	protected btnAppLauncher = new ElementWrapper(by.xpath("//a[@id='launcher']/span"));
	protected btnLaunchMax = new ElementWrapper(by.xpath("//a[@id='max']"));
	protected cbxAgentOptions = new ElementWrapper(by.xpath("//div[@id='simple-dropdown']"));
	protected optLogout = new ElementWrapper(by.xpath("//a[@id='Logout']"));
	protected btnConfirmLogout = new ElementWrapper(by.xpath("//button[text()='Yes']"));
	protected btnNavigationIcon = new ElementWrapper(by.xpath("//a[@id='module-picker-link']"));
	protected btnNavigationSearch = new ElementWrapper(by.xpath("//button[@id='select-search']"));
	protected btnACD = new ElementWrapper(by.xpath("//button[@id='select-acd']"));

	protected constructor() {

	}
	/**
	 * Log out
	 * @returns {Promise<LoginPage>} Login
	 * @memberof TopMenu
	 */
	public async logOut(): Promise<LoginPage> {
		await Logger.write(FunctionType.UI, "Logging out from Central");
		await this.cbxAgentOptions.click();
		await this.optLogout.click();
		await this.btnConfirmLogout.click();
		return LoginPage.getInstance();
	}

	/**
	 * Launch MAX
	 * @returns {Promise<MaxPage>} MAX page
	 * @memberof TopMenu
	 */
	public async launchMax(): Promise<MaxPage> {
		await Logger.write(FunctionType.UI, "Launching Max");
		await this.btnAppLauncher.click();
		await this.btnLaunchMax.click();
		return await MaxPage.getInstance();
	}

	/**
	 * Check evolve home page is displayed or not
	 * @returns {Promise<boolean>} the existence of tenant page
	 * @memberof TopMenu
	 */
	public async isPageDisplayed(): Promise<boolean> {
		return await this.btnAppLauncher.isDisplayed();
	}

	/**
	 * Go to search page 
	 * @returns {Promise<any>} search page
	 * @memberof TopMenu
	 */
	public async gotoSearchPage(): Promise<any> {
		await Logger.write(FunctionType.UI, "Going to Search page");
		await this.btnNavigationIcon.click();
		await this.btnNavigationSearch.click();
		let sp = require("./search-page").default;
		return sp.getInstance();
	}

	/**
	 * Go to ACD page 
	 * @returns {Promise<any>} ACD page
	 * @memberof TopMenu
	 */
	public async gotoACDPage(): Promise<any> {
		await Logger.write(FunctionType.UI, "Going to ACD page");
		await this.btnNavigationIcon.click();
		this.btnACD.click();
		let ACDPage = require("../acd/acd-page").default;
		return ACDPage.getInstance();
	}

	public async waitForPageLoad(): Promise<void> {
		await this.cbxAgentOptions.wait();
	}
}