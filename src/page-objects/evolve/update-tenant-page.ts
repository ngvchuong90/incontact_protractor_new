import { by } from "protractor";
import TopMenu from '../evolve/top-menu';
import { Logger, LogType, FunctionType } from '../../utilities/logger';
import ElementWrapper from '../../utilities/protractor-wrappers/element-wrapper';
import BusinessUnitDetailsPage from "./business-unit-details-page";

export default class UpdateTenantPage extends TopMenu {

    private static _updateTenantPage: UpdateTenantPage = null;

    protected btnMoreSetting = new ElementWrapper(by.xpath("//button[@id='more-settings-button']"));
    protected optionImpersonateAndConfigure = new ElementWrapper(by.xpath("//ul[@id='more-settings-dropdown']/li[@id='launchImpersonation']"));
    protected optionImpersonateAndSupport = new ElementWrapper(by.xpath("//ul[@id='more-settings-dropdown']/li[@id='launchSupportImpersonation']"));
    protected optionDeactivateAccount = new ElementWrapper(by.xpath("//ul[@id='more-settings-dropdown']/li[@id='toggleStatus']"));
    protected rowSpinner = new ElementWrapper(by.xpath('//div[@class="row wfmspinner"]'));

    constructor() { super(); }

    public static getInstance(): UpdateTenantPage {
        // this._updateTenantPage = (this._updateTenantPage == null) ? new UpdateTenantPage() : this._updateTenantPage;
        this._updateTenantPage = new UpdateTenantPage();
        return this._updateTenantPage;
    }

    /**
     * Check tenant details page is displayed or not
     * @returns {Promise<boolean>} the existence of tenant detail page
     * @memberof UpdateTenantPage
     */
    public async isTenantDetailPageDisplayed(): Promise<boolean> {
        return await this.btnMoreSetting.isDisplayed();
    }

    /**
     * Show more setting options by slclicking more setting option
     * @returns {Promise<this>} Update Tenant Page
     * @memberof UpdateTenantPage
     */
    public async showMoreSettingOption(): Promise<this> {
        await Logger.write(FunctionType.UI, "Showing more setting options");
        await this.rowSpinner.waitUntilDisappear();
        await this.btnMoreSetting.click();
        return this;
    }

    /**
     * Check more setting option is displayed or not
     * @returns {Promise<boolean>} the existence of impersonate and configure option and impersonate and support option
     * @memberof UpdateTenantPage
     */
    public async isMoreSettingOptionDisplayed(): Promise<boolean> {
        return (await this.optionImpersonateAndConfigure.isDisplayed() && await this.optionImpersonateAndSupport.isDisplayed());
    }

    /**
     * Click impersonate and confiruge option in more setting dropdown
     * @returns {Promise<BusinessUnitDetailsPage>} Business Unit Details page
     * @memberof UpdateTenantPage
     */
    public async selectImpersonateAndConfigureDetailsItem(): Promise<BusinessUnitDetailsPage> {
        await Logger.write(FunctionType.UI, "Selecting impersonte and configure option");
        await this.optionImpersonateAndConfigure.click();
        return BusinessUnitDetailsPage.getInstance();
    }
}
