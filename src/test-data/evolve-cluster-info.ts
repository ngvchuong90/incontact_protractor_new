export let evolveClusters: object = {
    "evolveClusters": [
        {
            "id": "TO31",
            "agents": [
                {
                    "type": "SuperAdmin",
                    "email": "tmadmtest@mailinator.com",
                    "password": "Aa123456",
                    "name": ""
                },
                {
                    "type": "Admin",
                    "email": "automation_adm@mailinator.com",
                    "password": "Password1",
                    "name": "automation adm"
                },
                {
                    "type": "Agent",
                    "email": "automation_agent1@mailinator.com",
                    "password": "Password1",
                    "name": "automation agent1"
                },
                {
                    "type": "Agent",
                    "email": "automation_agent2@mailinator.com",
                    "password": "Password1",
                    "name": "automation agent2"
                },
                {
                    "type": "Agent",
                    "email": "automation_agent3@mailinator.com",
                    "password": "Password1",
                    "name": "automation agent3"
                }
            ],
            "outboundNumber": "9990370014",
            "inboundNumber": "9990370006",
            "loginUrl": "https://auth.test.nice-incontact.com/login/#/",
            "tenantUrl": "https://na1.test.nice-incontact.com/tm/#/tenants",
            "evolveUrl": "https://na1.test.nice-incontact.com/",
            "evolveHomeUrl": "https://home-to31.test.nice-incontact.com/",
            "activateUrl": "https://na1.test.nice-incontact.com/login/#/setPassword?email=",
            "tokenUrl": "https://api-to31.test.nice-incontact.com/InContactAuthorizationServer/Token",
            "encoded": "Basic aW5Db250YWN0IFN0dWRpb0BpbkNvbnRhY3QgSW5jLjpaV0kxTUdFNE5tWTNOR1JsTkdJMU9EbG1ZMk14TUdFM1pUUmhOek0xWkRNPQ==",
            "Ibskill": "IBPhoneSkillAutomation",
            "Obskill": "OBPhoneSkillAutomation",
            "tenantName": "perm_automation_TO31"
        },
        {
            "id": "TO32",
            "agents": [
                {
                    "type": "SuperAdmin",
                    "email": "tmadmtest@mailinator.com",
                    "password": "Aa123456",
                    "name": ""
                },
                {
                    "type": "Admin",
                    "email": "automation_adm_to32@mailinator.com",
                    "password": "Password1",
                    "name": "Automation Admin"
                },
                {
                    "type": "Agent",
                    "email": "automation_agent1_to32@mailinator.com",
                    "password": "Password1",
                    "name": "Agent1 Automation"
                },
                {
                    "type": "Agent",
                    "email": "automation_agent2_to32@mailinator.com",
                    "password": "Password1",
                    "name": "Agent2 Automation"
                },
                {
                    "type": "Agent",
                    "email": "automation_agent3_to32@mailinator.com",
                    "password": "Password1",
                    "name": "Agent3 Automation"
                }
            ],
            "outboundNumber": "9990370014",
            "inboundNumber": "9990370006",
            "loginUrl": "https://auth.test.nice-incontact.com/login/#/",
            "tenantUrl": "https://na1.test.nice-incontact.com/tm/#/tenants",
            "evolveUrl": "https://na1.test.nice-incontact.com/",
            "evolveHomeUrl": "https://home-to32.test.nice-incontact.com/",
            "activateUrl": "https://na1.test.nice-incontact.com/login/#/setPassword?email=",
            "tokenUrl": "https://api-to32.test.nice-incontact.com/InContactAuthorizationServer/Token",
            "encoded": "Basic aW5Db250YWN0IFN0dWRpb0BpbkNvbnRhY3QgSW5jLjpaV0kxTUdFNE5tWTNOR1JsTkdJMU9EbG1ZMk14TUdFM1pUUmhOek0xWkRNPQ==",
            "Ibskill": "IBPhoneSkillAutomation",
            "Obskill": "OBPhoneSkillAutomation",
            "tenantName": "perm_automation_TO32"
        },
        {
            "id": "SO31",
            "agents": [
                {
                    "type": "SuperAdmin",
                    "email": "tmadmstaging@mailinator.com",
                    "password": "cdyEX!4Mm!GWe9e",
                    "name": ""
                },
                {
                    "type": "Admin",
                    "email": "adm.so31@mailinator.com",
                    "password": "Password1",
                    "name": "Admin SO31"
                },
                {
                    "type": "Agent",
                    "email": "agent1.so31@mailinator.com",
                    "password": "Password1",
                    "name": "Agent1 SO31"
                },
                {
                    "type": "Agent",
                    "email": "Agent2.so31@mailinator.com",
                    "password": "Password1",
                    "name": "Agent2 SO31"
                },
                {
                    "type": "Agent",
                    "email": "Agent3.so31@mailinator.com",
                    "password": "Password1",
                    "name": "Agent3 SO31"
                }
            ],
            "outboundNumber": "9990370014",
            "inboundNumber": "9990370006",
            "loginUrl": "https://auth.staging.nice-incontact.com/login/#/",
            "tenantUrl": "https://na1.staging.nice-incontact.com/tm/#/tenants",
            "evolveUrl": "https://na1.staging.nice-incontact.com/",
            "evolveHomeUrl": "https://home-so31.test.nice-incontact.com/",
            "activateUrl":"https://na1.staging.nice-incontact.com/login/#/setPassword?email=",
            "tokenUrl": "https://api-so31.staging.nice-incontact.com/InContactAuthorizationServer/Token",
            "encoded": "Basic aW5Db250YWN0IFN0dWRpb0BpbkNvbnRhY3QgSW5jLjpaV0kxTUdFNE5tWTNOR1JsTkdJMU9EbG1ZMk14TUdFM1pUUmhOek0xWkRNPQ==",
            "Ibskill": "IBPhoneSkillAutomation",
            "Obskill": "OBPhoneSkillAutomation",
            "tenantName": "Perm Automation staging"
        },
        {
            "id": "SO32",
            "agents": [
                {
                    "type": "SuperAdmin",
                    "email": "tmadmstaging@mailinator.com",
                    "password": "cdyEX!4Mm!GWe9e",
                    "name": ""
                },
                {
                    "type": "Admin",
                    "email": "adm_so32Load100007@mailinator.com",
                    "password": "Mcsen!2hqrNAUaek",
                    "name": "Admin so32Load100007"
                },
                {
                    "type": "Agent",
                    "email": "test_automation1.so32Load100007@mailinator.com",
                    "password": "cdyEX!4Mm!GWe9e2",
                    "name": "test automation1"
                },
                {
                    "type": "Agent",
                    "email": "test_automation2.so32Load100007@mailinator.com",
                    "password": "cdyEX!4Mm!GWe9e2",
                    "name": "test automation2"
                },
                {
                    "type": "Agent",
                    "email": "test_automation3.so32Load100007@mailinator.com",
                    "password": "cdyEX!4Mm!GWe9e2",
                    "name": "test automation3"
                }
            ],
            "outboundNumber": "9990370014",
            "inboundNumber": "9990370006",
            "loginUrl": "https://auth.staging.nice-incontact.com/login/#/",
            "tenantUrl": "https://na1.staging.nice-incontact.com/tm/#/tenants",
            "evolveUrl": "https://na1.staging.nice-incontact.com/",
            "evolveHomeUrl": "https://home-so32.test.nice-incontact.com/",
            "activateUrl":"https://na1.staging.nice-incontact.com/login/#/setPassword?email=",
            "tokenUrl": "https://api-so32.staging.nice-incontact.com/InContactAuthorizationServer/Token",
            "encoded": "Basic aW5Db250YWN0IFN0dWRpb0BpbkNvbnRhY3QgSW5jLjpaV0kxTUdFNE5tWTNOR1JsTkdJMU9EbG1ZMk14TUdFM1pUUmhOek0xWkRNPQ==",
            "Ibskill": "IBPhoneSkillAutomation",
            "Obskill": "OBPhoneSkillAutomation",
            "tenantName": "perm SO32Load 100007"
        }
    ]
}