import { AgentType, Agent, AgentRole } from "../../data-objects/agent";
import LoginPage from "../../page-objects/evolve/login-page";
import TestRunInfo from "../../data-objects/test-run-info";
import BrowserWrapper from "../../utilities/protractor-wrappers/browser-wrapper";
import EmployeesPage from "../../page-objects/evolve/employee-page";
import { EvolveCluster } from "../../data-objects/cluster";

describe("Create New Employee", function () {

    let evolveCluster: EvolveCluster = TestRunInfo.cluster;
    let admin: Agent = evolveCluster.getAdmin();
    let employee: Agent = new Agent().initData(AgentRole.Agent);

    beforeEach(async () => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = TestRunInfo.testTimeout;
        await BrowserWrapper.restart();
        await BrowserWrapper.maximize();
        await BrowserWrapper.get(evolveCluster.evolveUrl);
    })

    it('429329 - Create New Employee', async () => {
        // 1. Login access to evolve.
        // 2. As an Admin user log in to Evolve WFO
        let loginPage: LoginPage = LoginPage.getInstance();
        let employeesPage: EmployeesPage = await loginPage.loginAsAdmin(admin);

        // VP: Check Employees page is displayed.
        expect(await employeesPage.isPageDisplayed()).toEqual(true);

        // 3. Click on New Employee button
        await employeesPage.openNewEmployeeForm();

        // VP: Check Create New Employee pop up is displayed
        expect(await employeesPage.isCreateNewEmployeeFormDisplayed()).toEqual(true);

        // 4. Enter data to create new employee
        await employeesPage.fillNewEmployeeForm(employee);

        // VP: Check Fields are filled with entered data
        await employeesPage.isEmployeeInfoCorrect(employee);

        // 5. Click Create Button
        await employeesPage.clickSaveButton();

        // VP: Check 'Employee was created successfully' green message is displayed at the bottom right
        // VP: Check action return Employee Page
        expect(await employeesPage.isSuccessMessageDisplayed()).toEqual(true);
        expect(await employeesPage.isPageDisplayed()).toEqual(true);

        // VP: The new employee is listed on the Employees list.
        await employeesPage.searchEmployeeName(employee.name);
        expect(await employeesPage.isNewEmployeeCreated(employee.email)).toEqual(true);

        // Delete employee
        await employeesPage.deleteEmployee(employee.email);

        // Logout Employee Page
        await employeesPage.logOut();
    });
});