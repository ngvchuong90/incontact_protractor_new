import { Tenant, TenantType, ApplicationType, Feature, RecordingSetting } from "../../data-objects/tenant";
import { Timezone } from "../../data-objects/cluster";
import UserTenant from "../../data-objects/user-tenant";
import TestRunInfo from "../../data-objects/test-run-info";
import LoginPage from "../../page-objects/evolve/login-page";
import { Agent, AgentType, AgentRole } from "../../data-objects/agent";
import BrowserWrapper from "../../utilities/protractor-wrappers/browser-wrapper";
import AddApplicationPage from "../../page-objects/evolve/add-application-page";
import EmployeesPage from "../../page-objects/evolve/employee-page";
import TenantPage from "../../page-objects/evolve/tenant-page";
import CreateTenantPage from "../../page-objects/evolve/create-tenant-page";

describe("Create Tenant", function () {
	let addApplicationPage: AddApplicationPage;
	let loginPage: LoginPage;
	let employeesPage: EmployeesPage;
	let tenantPage: TenantPage;
	let createTenantPage: CreateTenantPage;
	let evolveCluster = TestRunInfo.cluster;
	let superAdmin: Agent = evolveCluster.getSuperAdmin();

	beforeEach(async () => {
		jasmine.DEFAULT_TIMEOUT_INTERVAL = TestRunInfo.testTimeout;
		await BrowserWrapper.restart();
		await BrowserWrapper.maximize();
		await BrowserWrapper.get(evolveCluster.tenantUrl);
	})

	it('Create Tenant', async () => {
		let tenant = new Tenant();
		let user = new UserTenant();
		let newAgent = new Agent();

		tenant.initData(TenantType.CUSTOMER, Timezone.PACIFIC_TIME, "-1", "-1");
		user.initData(tenant.name);
		newAgent.email = user.email;
		newAgent.password = user.password;

		//Login page will be displayed
		loginPage = LoginPage.getInstance();
		expect(await loginPage.isPageDisplayed()).toEqual(true);

		//User will be redirected to the Tenants management page
		tenantPage = await loginPage.loginAsSuperAdmin(superAdmin);
		expect(await tenantPage.isPageDisplayed()).toEqual(true);

		//Create New Tenant page will be displayed
		createTenantPage = await tenantPage.openCreateTenantPage();
		expect(await createTenantPage.isPageDisplayed()).toEqual(true);

		//Fields are filled
		await createTenantPage.gotoGeneralTab();
		await createTenantPage.fillGeneralTab(tenant);
		expect(tenant.name).toEqual(await createTenantPage.getEnteredName());
		expect(tenant.tenantType).toEqual(await createTenantPage.getSelectedTenantType());
		expect(tenant.timezone).toEqual(await createTenantPage.getSelectedTimezone());
		expect(tenant.billingId).toEqual(await createTenantPage.getEnteredBillingId());
		expect(tenant.clusterId).toEqual(await createTenantPage.getEnteredClusterId());
		expect(tenant.billingCycle).toEqual(await createTenantPage.getEnteredBillingCycle());
		expect(tenant.billingTelephoneNumber).toEqual(await createTenantPage.getEnteredBillingTelephoneNumber());
		expect(tenant.userCap).toEqual(await createTenantPage.getEnteredUserCap());

		//Add Applications page is displayed
		await createTenantPage.gotoApplicationsFeaturesTab();
		addApplicationPage = await createTenantPage.addApplication();
		expect(await addApplicationPage.isPageDisplayed()).toEqual(true);

		//In step 2 you will see "QM" check box checked
		await addApplicationPage.selectApplication(ApplicationType.QM);
		await addApplicationPage.gotoFeatureTab();
		expect(await addApplicationPage.isQmEvolveCheckboxChecked()).toEqual(true);

		//QM Setting page is displayed
		await addApplicationPage.gotoConfigureSettingsTab();
		expect(await addApplicationPage.isQMSettingPageDisplayed()).toEqual(true);

		// Step3 Configure Settings is displayed and CXOne is the value selected for the combobox
		await addApplicationPage.selectRecordingSource(RecordingSetting.CXONE);
		expect(await addApplicationPage.isConfigSettingSelected(RecordingSetting.CXONE)).toEqual(true);

		//QM is listed in the Applications & Features tab 
		createTenantPage = await addApplicationPage.finishAddApplication();
		expect(await createTenantPage.isNewApplicationDisplayed(ApplicationType.QM)).toEqual(true);

		//Fields are filled
		await createTenantPage.gotoUsersTab();
		await createTenantPage.fillUserTab(user);
		expect(user.firstName).toEqual(await createTenantPage.getEnteredFirstName());
		expect(user.lastName).toEqual(await createTenantPage.getEnteredLastName());
		expect(user.username).toEqual(await createTenantPage.getEnteredUsername());
		expect(user.email).toEqual(await createTenantPage.getEnteredEmailAddress());
		expect(user.password).toEqual(await createTenantPage.getEnteredPassword());
		expect(user.confirmPassword).toEqual(await createTenantPage.getEnteredConfirmPassword());

		//The new tenant will be added to the tenants list, and the status of "Active" 
		tenantPage = await createTenantPage.createAndActivateTenant();
		expect(await tenantPage.isNewTenantDisplayed(tenant.name)).toEqual(true);

		//User is able to login with the credentials
		loginPage = await tenantPage.logOut();
		employeesPage = await loginPage.loginAsAdmin(newAgent);
		expect(await employeesPage.isPageDisplayed()).toEqual(true);

		await employeesPage.logOut();
	});
})