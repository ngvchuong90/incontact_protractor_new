import { Tenant } from "../../data-objects/tenant";
import TestRunInfo from "../../data-objects/test-run-info";
import LoginPage from "../../page-objects/evolve/login-page";
import { Agent, AgentType } from "../../data-objects/agent";
import BrowserWrapper from "../../utilities/protractor-wrappers/browser-wrapper";
import TenantPage from "../../page-objects/evolve/tenant-page";
import UpdateTenantPage from "../../page-objects/evolve/update-tenant-page";
import BusinessUnitDetailsPage from "../../page-objects/evolve/business-unit-details-page";

describe("Impersonate and configure", function () {
    let loginPage: LoginPage;
    let evolveCluster = TestRunInfo.cluster;
    let superAdmin: Agent = evolveCluster.getSuperAdmin();
    let tenantPage: TenantPage;
    let updateTenantPage: UpdateTenantPage;
    let businessUnitDetailsPage: BusinessUnitDetailsPage;

    beforeEach(async () => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = TestRunInfo.testTimeout;
        await BrowserWrapper.restart();
        await BrowserWrapper.maximize();
        await BrowserWrapper.get(evolveCluster.tenantUrl);
    })

    it('Impersonate and configure', async () => {
        loginPage = LoginPage.getInstance();

        //1. Login page will be displayed
        expect(await loginPage.isPageDisplayed()).toEqual(true);

        //2. User will be redirected to the Tenants management page
        tenantPage = await loginPage.loginAsSuperAdmin(superAdmin);
        expect(await tenantPage.isPageDisplayed()).toEqual(true);

        //3. Tenant details page is displayed
        updateTenantPage = await tenantPage.selectTenant(evolveCluster.tenantName);
        expect(await updateTenantPage.isTenantDetailPageDisplayed()).toEqual(true);

        //4. Make sure the following options are displayed Impersonate & Configure, Impersonate & Support
        await updateTenantPage.showMoreSettingOption();
        expect(await updateTenantPage.isMoreSettingOptionDisplayed()).toEqual(true);

        //5. Verify that you are able to see with details tab in the Impersonate and Configure page
        businessUnitDetailsPage = await updateTenantPage.selectImpersonateAndConfigureDetailsItem();
        expect(await businessUnitDetailsPage.isBusinessUnitDetailsPageDisplayed()).toEqual(true);

        await businessUnitDetailsPage.logOut();
    });

})
