import TestRunInfo from "../../data-objects/test-run-info";
import LoginPage from "../../page-objects/evolve/login-page";
import { Agent, AgentType } from "../../data-objects/agent";
import EvolveAPIs from "../../apis/evolve-apis";
import BrowserWrapper from "../../utilities/protractor-wrappers/browser-wrapper";
import { MaxConnectOption, MaxState, SearchTimeRange, SearchColumnName, EvolveCluster } from "../../data-objects/cluster";
import SearchPage from "../../page-objects/evolve/search-page";
import MaxPage from "../../page-objects/evolve/max-page";
import EmployeesPage from "../../page-objects/evolve/employee-page";
import { StringUtility } from "../../utilities/utility";

describe("Inbound Call Agent hangs up call", function () {

    let loginPage: LoginPage;
    let evolveCluster: EvolveCluster = TestRunInfo.cluster;
    let admin: Agent = evolveCluster.getAdmin();
    let agent: Agent = evolveCluster.getRandomAgent();

    beforeEach(async () => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = TestRunInfo.testTimeout;
        await BrowserWrapper.restart();
        await BrowserWrapper.maximize();
        await BrowserWrapper.get(evolveCluster.evolveUrl);
        await EvolveAPIs.authorize(agent, evolveCluster.tokenUrl, evolveCluster.encoded);
        await EvolveAPIs.startOrJoinSession(agent, agent.phoneNumber);
        await EvolveAPIs.makeInboundCall(agent, evolveCluster.Ibskill);
    })

    it('Inbound Call Agent hangs up call', async () => {
        // Supervisor logged in
        loginPage = await LoginPage.getInstance();
        let employeesPage: EmployeesPage = await loginPage.loginAsAdmin(admin);
        expect(await employeesPage.isPageDisplayed()).toEqual(true);

        // Phone number text box is filled correctly
        let maxPage: MaxPage = await employeesPage.launchMax();
        await maxPage.enterLaunchForm(MaxConnectOption.PHONE, admin.phoneNumber, false);
        let actualPhoneNumber = await maxPage.getEnteredPhone(MaxConnectOption.PHONE);
        expect(admin.phoneNumber).toMatch(actualPhoneNumber);

        // MAX agent is connected and Agent status is Unavailable
        await maxPage.connectMax();
        expect(maxPage.isMaxLauched()).toEqual(true);
        expect(maxPage.getAgentStatus()).toMatch(MaxState.UNAVAILABLE.toUpperCase());

        // Status change to Avaiable
        await maxPage.changeState(MaxState.AVAILABLE);
        expect(await maxPage.getAgentStatus()).toMatch(MaxState.AVAILABLE.toUpperCase());

        // Incoming call is displayed in MAX Agent
        expect(await maxPage.isCallAccepted()).toEqual(true);

        // Call ends
        await maxPage.endCall();
        expect(await maxPage.isCallEnded()).toEqual(true);

        // Close MAX
        await maxPage.logOut();

        // The Search page is displayed
        let searchPage: SearchPage = await employeesPage.gotoSearchPage();
        expect(searchPage.isPageDisplayed()).toEqual(true);

        // All agents who made calls are listed
        searchPage.search(SearchTimeRange.TODAY, "");

        let expectedAgents = StringUtility.sortStrings(agent.name, admin.name);
        expect(expectedAgents).toMatch(await searchPage.getSearchResult(1, SearchColumnName.AGENT_NAME));

        // The NICE SaasS player is launch and you are able to see: Agent Name,
        // Customer Name, Duration, Direction, Start Time, End Time
        let player = await searchPage.playRecord(1);
        let isPlayerDisplayed = await player.isInfoDisplayedCorrectly(admin.name, agent.name);
        expect(isPlayerDisplayed).toEqual(true);

        // Player is closed
        await player.close();
        let currentTotalWindows = await BrowserWrapper.getTotalWindows();

        expect(1).toEqual(currentTotalWindows);

        await searchPage.logOut();
    });
})
