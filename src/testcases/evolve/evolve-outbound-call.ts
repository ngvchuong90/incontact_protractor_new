import TestRunInfo from "../../data-objects/test-run-info";
import { Agent, AgentType, AgentRole } from "../../data-objects/agent";
import LoginPage from "../../page-objects/evolve/login-page";
import BrowserWrapper from "../../utilities/protractor-wrappers/browser-wrapper";
import { MaxConnectOption, MaxState, SearchTimeRange, SearchColumnName, EvolveCluster } from "../../data-objects/cluster";
import { StringUtility, JsonUtility } from "../../utilities/utility";
import EmployeesPage from "../../page-objects/evolve/employee-page";
import MaxPage from "../../page-objects/evolve/max-page";
import SearchPage from "../../page-objects/evolve/search-page";
import InteractionPlayer from "../../page-objects/evolve/interaction-player";
import EvolveAPIs from "../../apis/evolve-apis";
import { APIResponse } from "utilities/api-core";


describe("Outbound Call Agent hangs up call", function () {

	let evolveCluster: EvolveCluster = TestRunInfo.cluster;
	let admin: Agent = evolveCluster.getAdmin();

	beforeEach(async () => {
		jasmine.DEFAULT_TIMEOUT_INTERVAL = TestRunInfo.testTimeout;

		await EvolveAPIs.authorize(admin, evolveCluster.tokenUrl, evolveCluster.encoded);		
		await BrowserWrapper.restart();
		await BrowserWrapper.maximize();
		await BrowserWrapper.get(evolveCluster.evolveUrl);
	})

	it('Outbound Call Agent hangs up call', async () => {
		// Supervisor logged in
		let loginPage: LoginPage = LoginPage.getInstance();
		let employeesPage: EmployeesPage = await loginPage.loginAsAdmin(admin);
		expect(await employeesPage.isPageDisplayed()).toEqual(true);

		// Phone number text box is filled correctly
		let maxPage: MaxPage = await employeesPage.launchMax();
		await maxPage.enterLaunchForm(MaxConnectOption.PHONE, admin.phoneNumber, false);

		let actualPhoneNumber: Promise<string> = maxPage.getEnteredPhone(MaxConnectOption.PHONE);
		expect(admin.phoneNumber).toMatch(actualPhoneNumber);

		// MAX agent is connected and Agent status is Unavailable
		maxPage.connectMax();
		expect(maxPage.isMaxLauched()).toEqual(true);
		expect(maxPage.getAgentStatus()).toMatch(MaxState.UNAVAILABLE.toUpperCase());

		// Status change to Available
		await maxPage.changeState(MaxState.AVAILABLE);
		expect(await maxPage.getAgentStatus()).toMatch(MaxState.AVAILABLE.toUpperCase());

		// Outbound call is displayed in MAX Agent
		await maxPage.callPhoneNumber(admin, evolveCluster.outboundNumber, evolveCluster.Obskill);
		expect(await maxPage.isCallAccepted()).toEqual(true);

		// Call ends
		await maxPage.endCall();
		expect(await maxPage.isCallEnded()).toEqual(true);

		// Close MAX
		await maxPage.logOut();

		// The Search page is displayed
		let searchPage: SearchPage = await employeesPage.gotoSearchPage();
		expect(searchPage.isPageDisplayed()).toEqual(true);

		// All agents who made calls are listed
		searchPage.search(SearchTimeRange.TODAY, "");
		expect(admin.name).toMatch(await searchPage.getSearchResult(1, SearchColumnName.AGENT_NAME));

		// The NICE SaasS player is launch and you are able to see: Agent Name,
		// Customer Name, Duration, Direction, Start Time, End Time
		let player: InteractionPlayer = await searchPage.playRecord(1);
		let isPlayerDisplayed: boolean = await player.isInfoDisplayedCorrectly(admin.name, "Customer");
		expect(isPlayerDisplayed).toEqual(true);

		// Player is closed
		await player.close();
		let currentTotalWindows: number = await BrowserWrapper.getTotalWindows();

		expect(1).toEqual(currentTotalWindows);

		await searchPage.logOut();
	});

})
