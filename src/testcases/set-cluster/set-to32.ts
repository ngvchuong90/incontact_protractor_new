import { JsonUtility, StringUtility } from "../../utilities/utility";
import * as fs from 'fs';
import { ConfigInfo } from "../../data-objects/config";

describe("Set SO32 Cluster", function () {

    it('Set SO32 Cluster', async () => {
        let jsonPath: string = "../../src/test-data/config-info.json";
        StringUtility.createConfigJSONFile(jsonPath);
        let jsonString: string = fs.readFileSync(jsonPath, 'utf8');
        let configData: ConfigInfo = JsonUtility.deserialize(JSON.parse(jsonString), ConfigInfo);
        configData.clusterId = "TO32";
        configData.browser = "Chrome";
        fs.writeFileSync(jsonPath, JSON.stringify(configData));
    });

})
