let XMLHttpRequest = require("XMLHttpRequest").XMLHttpRequest;

export class APICore {
	/**
	 * Send API request with data/body
	 * @static
	 * @param {Options} options 
	 * @returns {Promise<APIResponse>} API response
	 * @memberof APICore
	 */

	static request(options: Options): Promise<APIResponse> {
		return new Promise(function (resolve, reject) {
			let http = new XMLHttpRequest();

			//Open request
			http.open(options.method.toString(), options.url, true);

			//Add headers
			options.headers.forEach((value: any, key: string) => {
				http.setRequestHeader(key, value);
			});

			http.send(options.getBodyString());

			http.onerror = function () {
				reject(Error(`Error while requesting API. Status code: ${http.status}. Body: ${http.responseText}.`));
			};

			http.onload = function () {
				resolve(new APIResponse(http.status, http.responseText));
			};
		});
	}
}

export enum Method {
	POST = "POST",
	PUT = "PUT",
	GET = "GET",
	DELETE = "DELETE"
}

export class Options {
	url: string;
	method: Method;
	headers: Map<string, any>;
	body: Map<string, any>;

	constructor(url: string,
		method: Method,
		headers: Map<string, any> = new Map<string, any>(),
		body: Map<string, any> = new Map<string, any>()
	) {
		this.url = url;
		this.method = method;
		this.headers = headers;
		this.body = body;
	}

	addHeader(key: string, value: any) {
		this.headers.set(key, value);
		// console.log(this.headers);
	}

	addBody(key: string, value: any) {
		this.body.set(key, value);
		// console.log(this.body);
	}

	// logInfo(): void {
	// 	console.log(this.url + " - " + this.method.toString() + " - " + this.headers);
	// 	console.log(this.getBodyString());

	// }

	getBodyString(): string {
		let result: string = "{";
		let i = 0;

		this.body.forEach((value: any, key: string) => {
			result += `"${key}": "${value}"`;
			i++;
			if (i < this.body.size) {
				result += ", ";
			}
		});

		result += "}";

		// console.log(result);
		return result;
	}

}

export class APIResponse {
	status: number;
	body: string;

	constructor(status: number, body: string) {
		this.status = status;
		this.body = body;
	}
}