import jasmineReporters from "../../node_modules/jasmine-reporters";
import Jasmine2HtmlReporter from "../../node_modules/protractor-jasmine2-html-reporter"
import protractor_1 from "../../node_modules/protractor";
import BrowserWrapper from "../utilities/protractor-wrappers/browser-wrapper";

export class ConfigReport {

    /**
     * Create xml report
     * @static
     * @memberof ConfigReport
     */
    static createXMLReport() {
        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: true,
            savePath: './test/reports',
            filePrefix: 'xmlresults'
        }));
        var fs = require('fs-extra');
        fs.emptyDir('./test/reports/screenshots/', function (err) {
        });
        jasmine.getEnv().addReporter({
            specDone: function (result) {
                if (result.status == 'failed') {
                    protractor_1.browser.getCapabilities().then(function (caps) {
                        var browserName = caps.get('browserName');
                        protractor_1.browser.takeScreenshot().then(function (png) {
                            var stream = fs.createWriteStream('./test/reports/screenshots/' + browserName + '-' + result.fullName + '.png');
                            stream.write(new Buffer(png, 'base64'));
                            stream.end();
                        });
                    });
                }
            }
        });
    }

    /**
     * Convert xml report to pie chart
     * @static
     * @memberof ConfigReport
     */
    static convertXMLtoPieChart() {
        var browserName, browserVersion;
        var capsPromise = BrowserWrapper.getCapabilities();
        capsPromise.then(function (caps) {
            browserName = caps.get('browserName');
            browserVersion = caps.get('version');
            var HTMLReport = require('protractor-html-reporter');
            let testConfig = {
                reportTitle: 'Test Execution Report',
                outputPath: './test/reports/',
                screenshotPath: 'screenshots',
                testBrowser: browserName,
                browserVersion: browserVersion,
                modifiedSuiteName: false,
                screenshotsOnlyOnFailure: true,
                cleanDestination: false,
            };
            new HTMLReport().from('./test/reports/xmlresults.xml', testConfig);
        });
    }

}