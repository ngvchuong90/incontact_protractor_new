import jsLogger from 'js-logger';

export enum LogType {
    DEBUG,
    INFO,
    WARN,
    ERROR
}

export enum FunctionType {
    UI = "UI",
    API = "API",
    OTHER = "OTHER"
}

export class Logger {

    /**
     * Write log to console with Logging level
     * @static
     * @param {(string | FunctionType)} messagetype message type (API, UI or OTHER)
     * @param {string} info string which will be written in the console
     * @param {LogType} [debugType=LogType.INFO] log type (DEBUG, INFO, WARN or ERROR)
     * @returns {Promise<void>} Log message
     * @memberof Logger
     */
    static write(messagetype: string | FunctionType, info: string, debugType: LogType = LogType.INFO): Promise<void> {
        return new Promise((resolve) => {
            let text = `${messagetype} >>>>> ${info}`;

            switch (debugType) {
                case LogType.DEBUG:
                    resolve(jsLogger.debug(text));
                    break;
                case LogType.INFO:
                    resolve(jsLogger.info(text));
                    break;
                case LogType.WARN:
                    resolve(jsLogger.warn(text));
                    break;
                case LogType.ERROR:
                    resolve(jsLogger.error(text));
                    break;
            }
        })
    }

}