import { browser, element, ExpectedConditions as until, ProtractorBy, ElementFinder, protractor, Session } from 'protractor';
import { Logger, LogType, FunctionType } from '../logger';
import TestRunInfo from '../../data-objects/test-run-info';

export default class BrowserWrapper {

    private static readonly _pageTimeout: number = TestRunInfo.pageTimeout;

    /**
     * Switch window
     * @static
     * @param {number} index index of selected window which start from 0
     * @returns {Promise<void>} 
     * @memberof BrowserHelper
     */
    static async switchWindow(index: number): Promise<void> {
        try {
            let handles = await protractor.browser.getAllWindowHandles();
            await protractor.browser.switchTo().window(handles[index]);
            await this.switchToDefaultContent();
        } catch (err) {
            await Logger.write(FunctionType.UI, `Error while switching window`, LogType.INFO);
            await Logger.write(FunctionType.UI, `Error: ${err.message}`, LogType.ERROR);
        }
    }

    /**
     * Switch to default content
     * @static
     * @returns {Promise<void>} 
     * @memberof BrowserHelper
     */
    static async switchToDefaultContent(): Promise<void> {
        try {
            await protractor.browser.switchTo().defaultContent();
        } catch (err) {
            await Logger.write(FunctionType.UI, `Error while switching to default content`, LogType.INFO);
            await Logger.write(FunctionType.UI, `Error: ${err.message}`, LogType.ERROR);
        }
    }

    /**
     * Get and return total of open windows
     * @static
     * @returns {Promise<number>} number of open windows
     * @memberof BrowserHelper
     */
    static async getTotalWindows(): Promise<number> {
        let result = -1;

        try {
            let handles = await protractor.browser.getAllWindowHandles();
            result = handles.length;
        } catch (err) {
            await Logger.write(FunctionType.UI, `Error while getting total windows`, LogType.INFO);
            await Logger.write(FunctionType.UI, `Error: ${err.message}`, LogType.ERROR);
        }

        return result;
    }

    /**
     * Maximize the browser window
     * @static
     * @returns {Promise<BrowserWrapper>} maximized window
     * @memberof BrowserWrapper
     */
    static async maximize(): Promise<BrowserWrapper> {
        await Logger.write(FunctionType.UI, `Maximizing window`, LogType.INFO);
        await protractor.browser.manage().window().maximize();
        return this;
    }

    /**
     * Get URL
     * @static
     * @param {string} url url want to navigate to 
     * @returns {Promise<BrowserWrapper>} 
     * @memberof BrowserWrapper
     */
    static async get(url: string): Promise<BrowserWrapper> {
        await Logger.write(FunctionType.UI, `Navigating to ${url}`, LogType.INFO);
        await protractor.browser.get(url);
        return this;
    }

    /**
     * Get number of all window handles
     * @static
     * @returns {Promise<string[]>} number of window hanldes
     * @memberof BrowserWrapper
     */
    static async getAllWindowHandles(): Promise<string[]> {
        return await protractor.browser.getAllWindowHandles();
    }

    /** 
     * Wait for number of windows opens
     * @static
     * @param {number} expectedNumberOfWindows number of windows want to open
     * @param {number} [timeout=this._pageTimeout] maximum time to wait 
     * @returns {Promise<BrowserWrapper>} 
     * @memberof BrowserWrapper
     */
    static async waitForNumberOfWindows(expectedNumberOfWindows: number, timeout: number = this._pageTimeout): Promise<BrowserWrapper> {
        let crWindows = await this.getTotalWindows();
        let step = 1;

        while (expectedNumberOfWindows != crWindows) {
            browser.sleep(step);
            timeout -= step;
            crWindows = await this.getTotalWindows();
        }

        return this;
    }

    /**
     * Quit browser
     * @static
     * @memberof BrowserWrapper
     */
    static async quit() {
        await protractor.browser.quit();
    }

    /**
     * Restart browser
     * @static
     * @param {boolean} [waitForAngularEnabled=false] wait for angular enabled mode
     * @memberof BrowserWrapper
     */
    static async restart(waitForAngularEnabled: boolean = false) {
        await Logger.write(FunctionType.UI, `Restarting browser`, LogType.INFO);
        await protractor.browser.restart();
        await protractor.browser.waitForAngularEnabled(waitForAngularEnabled);
    }

    /**
     * Get information of browser 
     * @static
     * @returns browser information
     * @memberof BrowserWrapper
     */
    static async getCapabilities() {
        let capabiltites = await protractor.browser.getCapabilities();
        return capabiltites;
    }

    /**
     * Execute script
     * @static
     * @param {(string | Function)} script script to execute
     * @param {...any[]} var_args input argument
     * @returns {Promise<{}>} 
     * @memberof BrowserWrapper
     */
    static async executeScript(script: string | Function, ...var_args: any[]): Promise<{}> {
        return await protractor.browser.executeScript(script, var_args);
    }

    /**
     * Switch to target frame
     * @static
     * @param {number} index index of frame
     * @returns {Promise<void>} expected frame
     * @memberof BrowserWrapper
     */
    static async switchToFrame(index: number): Promise<void> {
        try {
            let handles = await protractor.browser.switchTo().frame(index);
        } catch (err) {
            await Logger.write(FunctionType.UI, `Error while switching frame`, LogType.INFO);
            await Logger.write(FunctionType.UI, `Error: ${err.message}`, LogType.ERROR);
        }
    }

    /**
     * Close browser
     * @static
     * @returns {Promise<void>} 
     * @memberof BrowserWrapper
     */
    static async close(): Promise<void> {
        try {
            await protractor.browser.close();
        } catch (err) {
            await Logger.write(FunctionType.UI, `Error while closing window`, LogType.INFO);
            await Logger.write(FunctionType.UI, `Error: ${err.message}`, LogType.ERROR);
        }
    }


}
