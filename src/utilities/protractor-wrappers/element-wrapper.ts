import { ElementFinder, by, By, element, browser, ExpectedConditions as until, Locator, protractor } from "protractor";
import { FunctionType, LogType, Logger } from "../logger";
import { promise, Key } from "selenium-webdriver";
import TestRunInfo from "../../data-objects/test-run-info";

export default class ElementWrapper {

    private _elementTimeout: number = TestRunInfo.elementTimeout;// in second
    private _by: Locator;
    private _element: ElementFinder;

    constructor(by: Locator) {
        this._by = by;
        this._element = element(this._by);
    }

    /**
     * Wait for element to be clickable
     * @param {number} [timeout=this._elementTimeout] maximum time to wait 
     * @returns {Promise<this>} 
     * @memberof ElementWrapper
     */
    public async wait(timeout: number = this._elementTimeout): Promise<this> {
        await protractor.browser.wait(until.elementToBeClickable(this._element), timeout * 1000).then(
            () => { },
            (error) => { });
        return this;
    }

    /**
     * Click and wait for click is effect
     * @returns {Promise<this>} 
     * @memberof ElementWrapper
     */
    public async click(): Promise<this> {
        await this.wait();

        await this._element.click().then(
            () => { },
            async (error) => {
                let _error: Error = <Error>error;
                if (_error.message.includes("Other element would receive the click")) {
                    await this.click();
                } else {
                    throw _error;
                }
            });
        return this;
    }

    /**
     * Clear textbox and input content to element
     * @param {(...(string | number | promise.Promise<string | number>)[])} var_args content to type
     * @returns {Promise<this>} 
     * @memberof ElementWrapper
     */
    public async type(...var_args: (string | number | promise.Promise<string | number>)[]): Promise<this> {
        await this.wait();
        await this._element.clear();

        var_args.forEach(async (var_arg) => {
            await this._element.sendKeys(<string>var_arg).then(
                () => { },
                (error) => { throw <Error>error; });
        });

        return this;
    }

    /**
     * Clear textbox element
     * @returns {Promise<this>} 
     * @memberof ElementWrapper
     */
    public async clear(): Promise<this> {
        await this.wait();
        await this.clear().then(
            () => { },
            (error) => { throw <Error>error; });

        return this;
    }

    /**
     * Input content to element
     * @param {(...(string | number | promise.Promise<string | number>)[])} var_args content to input
     * @returns {Promise<this>} 
     * @memberof ElementWrapper
     */
    public async sendKeys(...var_args: (string | number | promise.Promise<string | number>)[]): Promise<this> {
        await this.wait();
        var_args.forEach(var_arg => {
            this._element.sendKeys(var_arg).then(
                () => { },
                (error) => { throw <Error>error; });
        });

        return this;
    }

    public check() { }

    public uncheck() { }

    /**
     * Check element displays or not
     * @returns {Promise<boolean>} true or false
     * @memberof ElementWrapper
     */
    public async isDisplayed(): Promise<boolean> {
        await this.wait();
        let result = await this._element.isDisplayed();
        return result;
    }

    /**
     * Get attribute
     * @param {string} attributeName name of attribute
     * @returns {Promise<string>} 
     * @memberof ElementWrapper
     */
    public async getAttribute(attributeName: string): Promise<string> {
        await this.wait();
        let result = await this._element.getAttribute(attributeName);
        return result;
    }

    /**
     * Wait for element disappears
     * @param {number} [timeoutInSecond=this._elementTimeout] maximum time to wait
     * @returns {Promise<this>} 
     * @memberof ElementWrapper
     */
    public async waitUntilDisappear(timeoutInSecond: number = this._elementTimeout): Promise<this> {
        await protractor.browser.wait(until.invisibilityOf(this._element), timeoutInSecond * 1000).then(
            () => { },
            (error) => { });

        return this;
    }

    /**
     * Get element content
     * @returns {Promise<string>} 
     * @memberof ElementWrapper
     */
    public async getText(): Promise<string> {
        await this.wait();
        let result: string = await this._element.getText();
        return result;
    }

    /**
     * Wait for property of element is changed
     * @param {string} property property of element
     * @param {number} [timeoutInSecond=this._elementTimeout] maximum time to wait
     * @returns {Promise<this>} 
     * @memberof ElementWrapper
     */
    public async waitUntilPropertyChange(property: string, timeoutInSecond: number = this._elementTimeout): Promise<this> {
        try {
            let previousValue = await this.getAttribute(property);
            let currentValue = previousValue;
            let step = 1;

            while (timeoutInSecond > 0 && (previousValue == currentValue)) {
                await protractor.browser.sleep(step * 1000);
                currentValue = await this.getAttribute(property);
                timeoutInSecond -= step;
            }

        } catch (err) {
            await Logger.write(FunctionType.UI, `Error while waiting for element's property to change`, LogType.INFO);
            await Logger.write(FunctionType.UI, `Error: ${err.message}`, LogType.ERROR);
        }

        return this;
    }

    /**
     * Wait until element's property stop changing
     * @static
     * @param {ElementFinder} elem an element to wait
     * @param {string} property property's name
     * @param {number} [timeoutInSecond=60] time to wait
     * @param {string} [errorMsg="Could not find element"] message if cannot find element
     * @returns {Promise<void>} 
     * @memberof BrowserHelper
     */
    public async waitUntilPropertyNotChange(property: string, timeoutInSecond: number = 60): Promise<this> {
        try {
            let previousValue = "previousValue";
            let currentValue = await this.getAttribute(property);
            let step = 5;

            while (timeoutInSecond > 0 && (previousValue != currentValue)) {
                previousValue = currentValue;
                currentValue = await this.getAttribute(property);

                await protractor.browser.sleep(step * 1000);
                timeoutInSecond -= step;
            }
        } catch (err) {
            await Logger.write(FunctionType.UI, `Error while waiting for element's property not change`, LogType.INFO);
            await Logger.write(FunctionType.UI, `Error: ${err.message}`, LogType.ERROR);
        }

        return this;
    }
}