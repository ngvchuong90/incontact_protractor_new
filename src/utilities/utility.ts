import uuidv4 from '../../node_modules/uuid/v4';
import { EvolveClusters, ClusterID } from '../data-objects/cluster';
import { JsonConvert, OperationMode, ValueCheckingMode } from 'json2typescript';
import { evolveClusters } from '../test-data/evolve-cluster-info';
import Imap from '../../node_modules/imap';
import TestRunInfo from "../data-objects/test-run-info";
import * as fs from 'fs';

export class StringUtility {

	/**
	 * 
	 * Sort unordered strings
	 * @static
	 * @param {string[]} restArgs strings to sort. For example, you can input string1, string 2, string 3 as params
	 * @returns {string} a string contains sorted inputs separated by comma ","
	 * @memberof StringUtility
	 */
	static sortStrings(...restArgs: string[]): string {
		return restArgs.sort().toString().replace(",", ", ");
	}

	/**
	 * Generate a string that contains random numbers
	 * @static
	 * @param {number} length length of the generated number
	 * @param {number} [min=-1] min value of the generated number. Ignore this param by entering -1
	 * @param {number} [max=-1] max value of the generated number. Ignore this param by entering -1
	 * @returns {number} a random number
	 * @memberof StringUtility
	 */
	static getRandomNumber(length: number, min: number = -1, max: number = -1): number {
		if (min == -1 && max == -1) {
			let firstNumber = Math.floor((Math.random() * 9) + 1);
			let result = "";

			for (let i = 1; i < length; i++) {
				result += Math.floor(Math.random() * 10).toString();
			}

			return parseInt(firstNumber + result);
		}
		else {
			return Math.floor(Math.random() * (max - min + 1) + min);
		}
	}

	/**
	 * Generate a string that contains random numbers and alphabet characters
	 * @static
	 * @param {number} length length of the string, must be longer than prefix
	 * @param {string} [prefix=""] text which will be put in front of the random string
	 * @returns {string} a random string with fixed length including prefix
	 * @memberof StringUtility
	 */
	static createRandomString(length: number, prefix: string = ""): string {
		if (length < 1 || isNaN(length) == true) {
			throw 'Invalid input. Should be a number greater than 0';
		}
		let uuid = uuidv4().replace(/-/g, '');
		let uuidLength = uuid.length;
		let prefixLength = prefix.length;

		if (length > uuidLength) {
			let repeatation = Math.ceil(length / uuidLength);
			uuid = uuid.repeat(repeatation);
		}

		return prefix + uuid.substring(0, length - prefixLength);
	}

	/**
	 * Create random phone number to use in Evolve environment
	 * @static
	 * @param {string} clusterID Evolve cluster ID
	 * @returns {string} a random telephone number
	 * @memberof StringUtility
	 */
	static createEvolveRandomPhone(clusterID: ClusterID | string): string {
		let clusterType = clusterID.substr(0, 1).toLowerCase();

		if (clusterType == "t") {
			return "400515000" + StringUtility.getRandomNumber(1);
		}
		else if (clusterType == "s") {
			return "40000100" + StringUtility.getRandomNumber(2);
		}
		else if (clusterType == "c") {
			return "777320549" + StringUtility.getRandomNumber(1);
		}

		return "";
	}

	/**
	 * Create new file
	 * @static
	 * @param 
	 * @memberof StringUtility
	 */
	public static createConfigJSONFile(filePath: string): void {
		let isFileExist: boolean = fs.existsSync(filePath);

		if (isFileExist == false) {
			let fileData: string = `{"clusterId":"","browser":"","elementTimeout":60,"pageTimeout":60,"testTimeout":180000}`;
			fs.writeFileSync(filePath, fileData);
		}
	}

}

export class JsonUtility {

	/**
	 * Convert json object to redefined object
	 * @static
	 * @param {*} json 
	 * @param {new () => any} classReference 
	 * @returns {*} 
	 * @memberof JsonUtility
	 */
	public static deserialize(json: any, classReference: new () => any): any {
		let jsonConvert = new JsonConvert();
		// jsonConvert.operationMode = OperationMode.LOGGING; // print some debug data
		jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
		jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

		try {
			return jsonConvert.deserialize(json, classReference);
		} catch (e) {
			console.log((<Error>e));
		}

	}

	/**
	 * Get value from a Json string using JSONPath
	 * @static
	 * @param {string} jsonString content of Json string
	 * @param {string} jsonPath path to Json node
	 * @returns {string} field value
	 * @memberof JsonUtility
	 */
	static getFieldValue(jsonString: string, jsonPath: string): string {
		let jsonObj = JSON.parse(jsonString);
		let returnValue = eval("jsonObj." + jsonPath);
		return JSON.stringify(returnValue);
	}

	/**
	 * Get field count from a Json string using JSONPath
	 * @static
	 * @param {string} jsonString content of Json string
	 * @param {string} jsonPath path to Json node
	 * @returns {string} field value
	 * @memberof JsonUtility
	 */
	static getFieldCount(jsonString: string, jsonPath: string): number {
		let jsonObj = JSON.parse(jsonString);
		let jsonFullString = eval("jsonObj." + jsonPath);
		return Object.keys(jsonFullString).length;
	}

	// /**
	//  * Deseriallize Evolve Cluster Info to TypeScript Object
	//  * @static
	//  * @returns {EvolveClusters} EvolveClusters Object
	//  * @memberof JsonUtility
	//  */
	// public static deserializeEvolve(): EvolveClusters {
	// 	let jsonConvert: JsonConvert = new JsonConvert();
	// 	jsonConvert.operationMode = OperationMode.LOGGING; // print some debug data
	// 	jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
	// 	jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

	// 	try {
	// 		return jsonConvert.deserialize(evolveClusters, EvolveClusters);
	// 	} catch (e) {
	// 		console.log((<Error>e));
	// 	}
	// }

	// /**
	//  * Seriallize TypeScript Evolve Object to JSON
	//  * @static
	//  * @param {EvolveClusters} EvolveClusters Evolve TypeScript Object
	//  * @returns {any} JSON
	//  * @memberof StringUtility
	//  */
	// public static serializeEvolve(evolveClusters: EvolveClusters): any {
	// 	let jsonConvert: JsonConvert = new JsonConvert();
	// 	jsonConvert.operationMode = OperationMode.LOGGING; // print some debug data
	// 	jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
	// 	jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

	// 	try {
	// 		return jsonConvert.serialize(evolveClusters);
	// 	} catch (e) {
	// 		console.log((<Error>e));
	// 	}
	// }
}

export class Gmail {

	protected evolveCluster = TestRunInfo.cluster;

	/**
	 * Get gmail body content
	 * @returns gmail content
	 * @memberof Gmail
	 */
	async getGmailBodyContent() {
		let imap = new Imap({
			user: 'testautomationincontact@gmail.com',
			password: 'Logigear123!',
			host: 'imap.gmail.com',
			port: 993,
			tls: true
		});

		await imap.connect();

		let imapOnceReady = new Promise((resolve, reject) => {
			imap.once('ready', () => {
				let imapOpenBox = new Promise((resolve, reject) => {
					imap.openBox('INBOX', true, async function (err, box) {
						var f = await imap.seq.fetch('1:1', {
							bodies: ['HEADER.FIELDS (FROM)', 'TEXT']
						});
						await f.on('message', async function (msg, seqno) {
							await msg.on('body', function (stream, info) {
								var buffer = '';
								stream.on('data', function (chunk) {
									buffer += chunk.toString('utf8');
								});
								stream.once('end', function () {
									console.log(buffer);
									resolve(buffer);
								});
							});
							await msg.once('end', function () { });
						});

						await f.once('error', function (err) {
							console.log('Fetch error: ' + err);
						});
						await f.once('end', function () {
							imap.end();
						});
					});
				});
				resolve(imapOpenBox);
			});
		});

		return await imapOnceReady;
	}

	/**
	 * Get link in body content
	 * @param {any} str body content
	 * @returns url link
	 * @memberof Gmail
	 */
	async getBodyLink(str) {
		let pos = await str.indexOf(this.evolveCluster.activateUrl);
		let newString = await str.substr(pos, 175);
		let linkWeb = await newString.substr(0, newString.lastIndexOf(">https") - 1)

		linkWeb = await linkWeb.split("=3D").join("=");
		linkWeb = await linkWeb.replace(/\n|\r/g, "");

		return await linkWeb.split("==").join("=")
	}

	/**
	 * Insert value
	 * @param {any} str string to be inserted
	 * @param {any} index location
	 * @param {any} value value to insert
	 * @returns new string
	 * @memberof Gmail
	 */
	insert(str, index, value) {
		return str.substr(0, index) + value + str.substr(index);
	}

	/**
	 * get random Gmail by index
	 * @returns random gmail
	 * @memberof Gmail
	 */
	getRandomGmailByIndex() {
		let today: number = (new Date().getTime());
		let index = Math.floor(Math.abs(today - 1525340325732));

		let email = "testautomationincontact";
		let hexIndex = index.toString(2);
		let hexIndexArray = hexIndex.split("");
		for (let i = 0; i < hexIndexArray.length; i++) {
			if (hexIndexArray[i] == "1") {
				email = this.insert(email, email.length - hexIndexArray.length + i, ".")

			}
		}

		if (email.startsWith(".") == true) {
			return email.substring(1, email.length) + "@gmail.com";
		}
		return email + "@gmail.com";
	}
}
